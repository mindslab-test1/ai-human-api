package ai.maum.ai_human_api.util.url

class MediaUrlExchanger {
    companion object {
        fun changeProfile(url: String, profileFrom: Profile, profileTo: Profile) =
            url.replace(profileFrom.uploadPath(), profileTo.uploadPath())

        enum class Profile {
            DEV,
            STG,
            PROD;

            fun uploadPath(): String =
                when (this) {
                    DEV -> "/service-dev/ai-human/display-dev"
                    STG -> "/service-dev/ai-human/display-stg"
                    PROD -> "/service-dev/ai-human/display-dev"
                }
        }
    }
}
