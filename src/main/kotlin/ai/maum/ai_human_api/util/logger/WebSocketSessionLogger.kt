package ai.maum.ai_human_api.util.logger

import ai.maum.ai_human_api.websocket.SessionData
import org.slf4j.Logger
import org.springframework.beans.factory.ObjectFactory

class WebSocketSessionLogger(
    private val logger: Logger
) {
    private var _sessionId: String? = null
    private var _sessionDataFactory: ObjectFactory<SessionData>? = null

    fun sessionId(): String {
        _sessionDataFactory?.let {
            return _sessionDataFactory!!.`object`.sessionId?.substring(0, 8) ?: "#unknown"
        }
        _sessionId?.let {
            return _sessionId!!
        }
        throw RuntimeException("Impossible sessionId")
    }

    constructor(sessionDataFactory: ObjectFactory<SessionData>, logger: Logger) : this(logger) {
        _sessionDataFactory = sessionDataFactory
    }

    constructor (sessionId: String, logger: Logger) : this(logger) {
        _sessionId = sessionId.substring(0, 8).padStart(8, ' ')
    }

    @SuppressWarnings
    infix fun trace(message: String) {
        logger.trace(sessionDataFormat(message))
    }

    @SuppressWarnings
    infix fun debug(message: String) {
        logger.debug(sessionDataFormat(message))
    }

    @SuppressWarnings
    infix fun info(message: String) {
        logger.info(sessionDataFormat(message))
    }

    @SuppressWarnings
    infix fun warn(message: String) {
        logger.warn(sessionDataFormat(message))
    }

    @SuppressWarnings
    infix fun error(message: String) {
        logger.error(sessionDataFormat(message))
    }

    private fun sessionDataFormat(message: String): String =
        "[${sessionId()}] $message"
}
