package ai.maum.ai_human_api.util.stomp

import ai.maum.ai_human_api.websocket.MessageHelper
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service

@Service
class MessageSender(
    private val simpMessagingTemplate: SimpMessagingTemplate
) {
    operator fun invoke(sessionId: String, messageResponse: Any, channelUrl: String = "/queue/human") {
        simpMessagingTemplate.convertAndSendToUser(
            sessionId, channelUrl, messageResponse, MessageHelper.createHeaders(sessionId)
        )
    }
}
