package ai.maum.ai_human_api.util.text

class BraceFilter {
    companion object {
        fun removeTripleBraceBlock(text: String): String =
            text.replace(Regex("\\{\\{\\{(.*?)}}}"), "")

        fun removeTripleBraces(text: String): String =
            text.replace(Regex("\\{\\{\\{(.*?)}}}"), "$1")
    }
}
