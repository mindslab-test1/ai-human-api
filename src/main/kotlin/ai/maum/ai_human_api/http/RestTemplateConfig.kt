package ai.maum.ai_human_api.http

import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import java.time.Duration

@Configuration
class RestTemplateConfig {
    @Bean
    fun restTemplate(): RestTemplate {
        val httpRequestFactory = HttpComponentsClientHttpRequestFactory()
        val httpClient = HttpClientBuilder.create().setMaxConnTotal(200).setMaxConnPerRoute(20).build()
        httpRequestFactory.httpClient = httpClient
        return RestTemplateBuilder()
            // timeout이 짧으면 엔진이 느려서 다 timeout됨
            .setConnectTimeout(Duration.ofSeconds(60))
            .setReadTimeout(Duration.ofSeconds(60))
            .requestFactory { httpRequestFactory }
            .build()
    }
}
