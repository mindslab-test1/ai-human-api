package ai.maum.ai_human_api.http.filter.logger

import ai.maum.ai_human_api.http.filter.dsl.FilterUrlWrapper
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Order(0)
@Component
class LoggerFilter : FilterUrlWrapper(LoggerFilterImpl()) {
    init {
        skipRules {
        }
    }
}
