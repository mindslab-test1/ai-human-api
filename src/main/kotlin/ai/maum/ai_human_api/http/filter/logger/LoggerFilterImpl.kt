package ai.maum.ai_human_api.http.filter.logger

import ai.maum.ai_human_api.http.filter.dsl.FilterImpl
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED

class LoggerFilterImpl : FilterImpl {
    @Throws(IOException::class, ServletException::class)
    override fun run(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        try {
        } catch (e: Exception) {
            response.status = SC_UNAUTHORIZED
            return
        }

        chain.doFilter(request, response)
    }
}
