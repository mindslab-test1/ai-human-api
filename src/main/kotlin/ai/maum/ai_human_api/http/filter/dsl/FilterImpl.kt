package ai.maum.ai_human_api.http.filter.dsl

import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

interface FilterImpl {
    fun run(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain)
}
