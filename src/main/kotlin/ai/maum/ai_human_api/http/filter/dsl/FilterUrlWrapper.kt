package ai.maum.ai_human_api.http.filter.dsl

import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

abstract class FilterUrlWrapper(private val wrappedFilter: FilterImpl) : OncePerRequestFilter() {
    private val ruleProcess = RuleProcess()
    private var ruleProcessPolicy = RuleProcess.RuleProcessPolicy.SKIP

    private fun assert(uri: String): Boolean =
        when (ruleProcessPolicy) {
            RuleProcess.RuleProcessPolicy.SKIP -> assertSkip(uri)
            RuleProcess.RuleProcessPolicy.ALLOW -> assertAllow(uri)
        }

    private fun assertSkip(uri: String): Boolean {
        if (ruleProcess.getSkipList().contains(uri))
            return false
        for (heading in ruleProcess.getSkipHeadingsList())
            if (uri.startsWith(heading))
                return false
        return true
    }

    private fun assertAllow(uri: String): Boolean {
        if (ruleProcess.getAllowList().contains(uri))
            return true
        for (heading in ruleProcess.getAllowHeadingsList())
            if (uri.startsWith(heading))
                return true
        return false
    }

    /**
     * Filter will not handle skipped URIs.
     */
    fun skipRules(description: RuleProcess.() -> Unit) {
        ruleProcessPolicy = RuleProcess.RuleProcessPolicy.SKIP
        description(ruleProcess)
    }

    /**
     * Filter will handle allowed URIs only.
     */
    fun allowRules(description: RuleProcess.() -> Unit) {
        ruleProcessPolicy = RuleProcess.RuleProcessPolicy.ALLOW
        description(ruleProcess)
    }

    @Throws(IOException::class, ServletException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        if (assert(request.requestURI))
            wrappedFilter.run(request, response, chain)
        else
            chain.doFilter(request, response)
    }
}
