package ai.maum.ai_human_api.http.controller.dsl

interface ExtendedController {
    fun handler(description: HandlerProcess.() -> Unit): ResponseType {
        val handlerProcess = HandlerProcess()
        description(handlerProcess)
        handlerProcess.handle()

        return handlerProcess.response!!
    }
}
