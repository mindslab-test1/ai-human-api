package ai.maum.ai_human_api.http.controller.dsl

@HandlerMarker
class HandlerProcess {
    var w = Statement.RUN
    var a = Statement.TRY

    var response: ResponseType? = null
    var handler: HandlerMethod? = null

    infix fun <T : HandlerMethod> Statement.handler(handler: T) = wrap {
        this@HandlerProcess.handler = handler
    }

    fun handle() {
        response = handler!!()
    }

    enum class Statement {
        RUN,
        TRY;

        fun wrap(block: () -> Unit): Statement {
            when (this) {
                RUN -> block()
                TRY -> {
                    try {
                        block()
                    } catch (e: Exception) {
                        // Swallow exceptions
                    }
                }
            }
            return this
        }
    }
}
