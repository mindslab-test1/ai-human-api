package ai.maum.ai_human_api.http.controller.concierge

import ai.maum.ai_human_api.websocket.poc.message.downstream.PocResponse
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ConciergePocController(
    private val simpMessagingTemplate: SimpMessagingTemplate
) {
    val logger = LoggerFactory.getLogger(this::class.java)

    @PostMapping("/api/poc/fd")
    fun channelFaceDetection(@RequestBody requestDto: ChannelFaceDetectionRequest): ResponseEntity<Unit> {
        simpMessagingTemplate.defaultDestination = "/queue/poc"
        logger.info("FD Call received ${requestDto.fd}")
        when (requestDto.fd) {
            0 -> simpMessagingTemplate.convertAndSend(PocResponse.getFdNot())
            1 -> simpMessagingTemplate.convertAndSend(PocResponse.getFdNear())
            2 -> simpMessagingTemplate.convertAndSend(PocResponse.getFdFar())
            else -> logger.error("Invalid FD input")
        }

        return ResponseEntity.ok(Unit)
    }
}
