package ai.maum.ai_human_api.http.controller.concierge

data class ChannelFaceDetectionRequest(
    val fd: Int
)
