package ai.maum.ai_human_api.http.controller

import ai.maum.ai_human_api.grpc.lipsync.LipSyncGrpcClient
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import org.slf4j.LoggerFactory
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

@RestController
class DevController {
    private val fakeSessionId = "REST-API"
    private val log = WebSocketSessionLogger(fakeSessionId, LoggerFactory.getLogger(this.javaClass))

    @PostMapping("/api/lipsync/run4k")
    fun run4k(
        @RequestParam(value = "text") text: String,
        @RequestParam(value = "background") background: MultipartFile,
        @RequestParam(value = "width") width: Int,
        @RequestParam(value = "height") height: Int
    ): ResponseEntity<ByteArray> {
        try {
            val lipSyncGrpcClient = LipSyncGrpcClient(fakeSessionId)

            val generatedAvatar = lipSyncGrpcClient.generateLipSyncAvatar(text, ByteArrayResource(background.bytes), width, height, false)

            return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(generatedAvatar.byteArray)
        } catch (e: Exception) {
            log error "${e.message}\n${e.stackTraceToString()}"
            throw e
        }
    }

    @PostMapping("/api/lipsync/digitaldesk4k")
    fun digitaldesk4k(
        @RequestParam(value = "apiId") apiId: String?,
        @RequestParam(value = "apiKey") apiKey: String?,
        @RequestParam(value = "image") image: MultipartFile?,
        @RequestParam(value = "model") model: String?,
        @RequestParam(value = "transparent") transparent: String?,
        @RequestParam(value = "resolution") resolution: String?,
        @RequestParam(value = "text") text: String
    ): ResponseEntity<ByteArray> {
        try {
            val lipSyncGrpcClient = LipSyncGrpcClient(fakeSessionId)

            val byteArray = ClassPathResource("background/969088.png").inputStream.readAllBytes()
            val resource: ByteArrayResource =
                // Do not use ?.let here
                // see https://github.com/bumptech/glide/issues/2923
                object : ByteArrayResource(byteArray) {
                    override fun getFilename(): String {
                        return "background.png"
                    }
                }

            val generatedAvatar = lipSyncGrpcClient.generateLipSyncAvatar(text, resource, 3840, 2160, false)

            return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(generatedAvatar.byteArray)
        } catch (e: Exception) {
            log error "${e.message}\n${e.stackTraceToString()}"
            throw e
        }
    }

    @PostMapping("/api/lipsync/concierge4k")
    fun concierge4k(
        @RequestParam(value = "apiId") apiId: String?,
        @RequestParam(value = "apiKey") apiKey: String?,
        @RequestParam(value = "image") image: MultipartFile?,
        @RequestParam(value = "model") model: String?,
        @RequestParam(value = "transparent") transparent: String?,
        @RequestParam(value = "resolution") resolution: String?,
        @RequestParam(value = "text") text: String
    ): ResponseEntity<ByteArray> {
        try {
            val lipSyncGrpcClient = LipSyncGrpcClient(fakeSessionId)

            val byteArray = ClassPathResource("background/concierge4k.png").inputStream.readAllBytes()
            val resource: ByteArrayResource =
                // Do not use ?.let here
                // see https://github.com/bumptech/glide/issues/2923
                object : ByteArrayResource(byteArray) {
                    override fun getFilename(): String {
                        return "background.png"
                    }
                }

            val generatedAvatar = lipSyncGrpcClient.generateLipSyncAvatar(text, resource, 2160, 3840, false)

            return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(generatedAvatar.byteArray)
        } catch (e: Exception) {
            log error "${e.message}\n${e.stackTraceToString()}"
            throw e
        }
    }
}
