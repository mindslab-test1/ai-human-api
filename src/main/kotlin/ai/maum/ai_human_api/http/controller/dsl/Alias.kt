package ai.maum.ai_human_api.http.controller.dsl

import org.springframework.http.ResponseEntity

typealias ResponseType = ResponseEntity<Any>
typealias HandlerMethod = () -> ResponseType

abstract class HandlerType : HandlerMethod

@DslMarker
annotation class HandlerMarker

class Alias
