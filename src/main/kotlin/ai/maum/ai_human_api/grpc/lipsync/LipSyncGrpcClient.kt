package ai.maum.ai_human_api.grpc.lipsync

import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import com.google.protobuf.ByteString
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import maum.brain.lipsync.v2.BrainLipsync.LipSyncInput
import maum.brain.lipsync.v2.BrainLipsync.LipSyncRequestKey
import maum.brain.lipsync.v2.BrainLipsync.StatusCode
import maum.brain.lipsync.v2.BrainLipsync.VideoResolution
import maum.brain.lipsync.v2.LipSyncGenerationGrpc
import org.slf4j.LoggerFactory
import org.springframework.core.io.ByteArrayResource
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class LipSyncGrpcClient(
    private val sessionId: String,
    private val lipSyncServerIp: String = "182.162.19.26",
    private val lipSyncServerPort: Int = 45103
) {
    private val log = WebSocketSessionLogger(sessionId, LoggerFactory.getLogger(this.javaClass))
/*
    private val lipSyncServerIp = "182.162.19.29" // m dev server
    private val lipSyncServerPort = 45001
    private val lipSyncServerIp = "10.122.64.82" // brain
    private val lipSyncServerPort = 45002
 */

    private val channel: ManagedChannel
    private val blockingStub: LipSyncGenerationGrpc.LipSyncGenerationBlockingStub
    private val asyncStub: LipSyncGenerationGrpc.LipSyncGenerationStub

    init {
        try {
            val channelBuilder = ManagedChannelBuilder.forAddress(lipSyncServerIp, lipSyncServerPort).usePlaintext()
            channel = channelBuilder.build()
            blockingStub = LipSyncGenerationGrpc.newBlockingStub(channel)
            asyncStub = LipSyncGenerationGrpc.newStub(channel)
        } catch (e: Exception) {
            // logger.error(e.toString())
            throw Exception("Failed to create GrpcTtsEndpoint object: Failed to create channel or stub.")
        }
    }

    fun generateLipSyncAvatar(text: String, background: ByteArrayResource, width: Int, height: Int, transparent: Boolean): ByteArrayResource {
        log info "text: $text"
        log info "width: $width"
        log info "height: $height"
        val videoResolution = VideoResolution.newBuilder().setWidth(width).setHeight(height).build()

        log info "before upload"
        val requestKey = upload(text, ByteString.copyFrom(background.byteArray), 0, videoResolution, transparent)

        log info "status check"
        while (!statusCheck(requestKey))
            Thread.sleep(200)

        log info "download"
        return download(requestKey)
    }

    private fun upload(text: String, background: ByteString, speakerId: Int, resolution: VideoResolution, transparent: Boolean): String {
        val finishLatch = CountDownLatch(1)
        lateinit var requestKey: String

        log info "upload: creating responseObserver"
        val responseObserver = object : StreamObserver<LipSyncRequestKey> {
            override fun onNext(value: LipSyncRequestKey?) {
                value?.let {
                    requestKey = it.requestKey
                }
            }

            override fun onError(t: Throwable?) {
                log info "grpc Error: $t"
                finishLatch.countDown()
            }

            override fun onCompleted() {
                finishLatch.countDown()
            }
        }

        log info "upload: starting upload rpc"
        val requestObserver = asyncStub.upload(responseObserver)

        log info "upload: sending request"
        try {
            val lipSyncInput = LipSyncInput
                .newBuilder()
                .setText(text)
                .setBackground(background)
                .setSpeakerId(speakerId)
                .setResolution(resolution)
                .setTransparent(transparent)
                .build()
            requestObserver.onNext(lipSyncInput)
        } catch (e: RuntimeException) {
            requestObserver.onError(e)
            throw e
        }
        requestObserver.onCompleted()

        finishLatch.await(10, TimeUnit.MINUTES)
        log info "upload: done; requestKey=$requestKey"

        return requestKey
    }

    private fun statusCheck(requestKey: String): Boolean {
        val lipSyncRequestKey = LipSyncRequestKey.newBuilder().setRequestKey(requestKey).build()

        try {
            val statusMsg = blockingStub.statusCheck(lipSyncRequestKey)
            if (statusMsg.statusCode !in arrayOf(StatusCode.NOT_YET, StatusCode.PROCESSING, StatusCode.DONE))
                throw RuntimeException("Status Code: ${statusMsg.statusCode}")
            if (statusMsg.statusCode == StatusCode.DONE)
                return true
            return false
        } catch (e: RuntimeException) {
            throw e
        }
    }

    private fun download(requestKey: String): ByteArrayResource {
        val lipSyncRequestKey = LipSyncRequestKey.newBuilder().setRequestKey(requestKey).build()

        try {
            val lipSyncResult = blockingStub.download(lipSyncRequestKey)
            var byteString = ByteString.EMPTY
            for (each in lipSyncResult) {
                byteString = byteString.concat(each.video)
            }

            return object : ByteArrayResource(byteString.toByteArray()) {
                override fun getFilename(): String {
                    return "$requestKey.mp4"
                }
            }
        } catch (e: RuntimeException) {
            throw e
        }
    }
}
