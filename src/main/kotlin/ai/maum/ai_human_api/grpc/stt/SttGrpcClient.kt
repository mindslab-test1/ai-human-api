package ai.maum.ai_human_api.grpc.stt

import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.util.stomp.MessageSender
import ai.maum.ai_human_api.websocket.poc.message.downstream.PocResponse
import com.google.protobuf.ByteString
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import maum.brain.w2l.SpeechToTextGrpc
import maum.brain.w2l.W2L.Speech
import maum.brain.w2l.W2L.Text
import maum.brain.w2l.W2L.TextSegment
import org.slf4j.LoggerFactory

class SttGrpcClient(
    private val sessionId: String,
    private val messageSender: MessageSender
) {
    private val log = WebSocketSessionLogger(sessionId, LoggerFactory.getLogger(this.javaClass))
/*
    private val sttServerIp = "211.234.118.4" // STT engine server
    private val sttServerPort = 15001
*/
    private val sttServerIp = "221.168.33.192" // STT engine server
    private val sttServerPort = 15001

    private val channel: ManagedChannel
    private var blockingStub: SpeechToTextGrpc.SpeechToTextBlockingStub
    private var asyncStub: SpeechToTextGrpc.SpeechToTextStub

    private val channelBuilder = ManagedChannelBuilder.forAddress(sttServerIp, sttServerPort).usePlaintext()
    private val responseObserver: StreamObserver<TextSegment>
    private val requestObserver: StreamObserver<Speech>

    init {
        try {
            channel = channelBuilder.build()

            blockingStub = SpeechToTextGrpc.newBlockingStub(channel)
            asyncStub = SpeechToTextGrpc.newStub(channel)

            responseObserver = object : StreamObserver<TextSegment> {
                override fun onNext(value: TextSegment?) {
                    value?.let {
                        log info "TextSegment arrived: ${value.start}, ${value.end}, ${value.txt}"
                        messageSender(
                            sessionId,
                            PocResponse.getBabble(PocResponse.Word(value.start, value.end, value.txt))
                        )
                    }
                }

                override fun onCompleted() {
                    log debug "STT onCompleted"

                    // finishLatch.countDown()
                }

                override fun onError(t: Throwable?) {
                    t?.let {
                        log debug "onError: ${t.message}\n${t.stackTraceToString()}"

                        // finishLatch.countDown()
                        throw t
                    }
                }
            }
            requestObserver = asyncStub.streamRecognize(responseObserver)
        } catch (e: Exception) {
            // logger.error(e.toString())
            throw Exception("Failed to create SttGrpcClient object: Failed to create channel or stub.")
        }
    }

    fun onNext(speech: Speech) {
        // val finishLatch = CountDownLatch(1)
        log debug "Speaking to STT: ${speech.bin.size()} ${speech.bin.substring(0, 24)}"

        try {
            requestObserver.onNext(speech)
        } catch (e: RuntimeException) {
            requestObserver.onError(e)
            throw e
        }
        // requestObserver.onCompleted()

        // finishLatch.await(1, TimeUnit.MINUTES)
    }

    companion object {
        fun newSpeech(byteString: ByteString): Speech =
            Speech.newBuilder().setBin(byteString).build()

        fun newText(text: String): Text =
            Text.newBuilder().setTxt(text).build()

        fun newTextSegment(start: Int, end: Int, text: String): TextSegment =
            TextSegment.newBuilder().setStart(start).setEnd(end).setTxt(text).build()
    }
}
