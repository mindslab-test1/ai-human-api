package ai.maum.ai_human_api.websocket

import ai.maum.ai_human_api.grpc.stt.SttGrpcClient
import ai.maum.ai_human_api.jpa.avatar.Avatar
import ai.maum.ai_human_api.jpa.background.Background
import ai.maum.ai_human_api.jpa.scenario.Scenario
import ai.maum.ai_human_api.jpa.talklog.TalkLog
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component

@Component
@Scope(scopeName = "websocket", proxyMode = ScopedProxyMode.TARGET_CLASS)
data class SessionData(
    var sessionId: String? = null,
    var flowId: Long? = null,
    var avatar: Avatar? = null,
    var background: Background? = null,
    var scenario: Scenario? = null,
    var talkLog: TalkLog? = null,
    var sttGrpcClient: SttGrpcClient? = null
)
