package ai.maum.ai_human_api.websocket

import org.springframework.messaging.MessageHeaders
import org.springframework.messaging.simp.SimpMessageHeaderAccessor
import org.springframework.messaging.simp.SimpMessageType

class MessageHelper {
    companion object {
        fun createHeaders(sessionId: String): MessageHeaders {
            val headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE)
            headerAccessor.sessionId = sessionId
            headerAccessor.setLeaveMutable(true)
            return headerAccessor.messageHeaders
        }
    }
}
