package ai.maum.ai_human_api.websocket.full.service.init

import ai.maum.ai_human_api.builder.flow.Flow
import ai.maum.ai_human_api.builder.license.License
import ai.maum.ai_human_api.jpa.avatar.AvatarRepository
import ai.maum.ai_human_api.jpa.background.BackgroundRepository
import ai.maum.ai_human_api.jpa.scenario.Scenario
import ai.maum.ai_human_api.jpa.scenario.ScenarioRepository
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.full.exception.AvatarDoesNotExistException
import ai.maum.ai_human_api.websocket.full.exception.BackgroundDoesNotExistException
import ai.maum.ai_human_api.websocket.full.exception.InvalidLicenseException
import ai.maum.ai_human_api.websocket.full.exception.ScenarioDoesNotExistException
import ai.maum.ai_human_api.websocket.full.message.downstream.FullResponse
import ai.maum.ai_human_api.websocket.full.message.upstream.InitInput
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class Init(
    private val flow: Flow,
    private val license: License,
    private val avatarRepository: AvatarRepository,
    private val backgroundRepository: BackgroundRepository,
    private val scenarioRepository: ScenarioRepository,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    private val specialProjectId = arrayOf(-100L, -200L, -300L, -400L, -500L, -2000L, -2100L, -5000L)

    operator fun invoke(message: InitInput, sessionId: String): FullResponse {
        log info "# Processing init message(full)"

        sessionDataFactory.`object`.sessionId = sessionId
        sessionDataFactory.`object`.flowId = message.projectId

        val flowData = flow.getFlowInfo(message.projectId)

        // Human profile check
        val avatar = avatarRepository.findTopByModel(flowData.avatar) ?: throw AvatarDoesNotExistException("Avatar ${flowData.avatar} does not exist")
        val background = backgroundRepository.findByIdOrNull(flowData.background) ?: throw BackgroundDoesNotExistException("Background ${flowData.background} does not exist")
        var scenarioExists = scenarioRepository.findTopByHost(flowData.scenario)
        if (scenarioExists == null) {
            scenarioExists = Scenario()
            scenarioExists.id = null
            scenarioExists.host = flowData.scenario
            scenarioRepository.save(scenarioExists)
        }
        val scenario = scenarioRepository.findTopByHost(flowData.scenario) ?: throw ScenarioDoesNotExistException("Scenario ${flowData.scenario} does not exist")

        sessionDataFactory.`object`.avatar = avatar
        sessionDataFactory.`object`.background = background
        sessionDataFactory.`object`.scenario = scenario

        log info
            "\nAvatar: ${flowData.avatar} -> ${avatar.id}: ${avatar.model}" +
            "\nScenario: ${flowData.scenario} -> ${scenario.id}: ${scenario.host}" +
            "\nBackground: ${flowData.background} -> ${background.id}: ${background.name}" +
            "\nMuteVideo: ${avatar.muteVideo}" +
            "\nProfileImage: ${avatar.profileImage}" +
            "\nPose: ${avatar.mutePose}"

        if (!specialProjectId.contains(message.projectId))
            if (!license.validateLicense(flowData.email).valid)
                throw InvalidLicenseException("User ${flowData.email} with project ID ${message.projectId} does not have valid license")

        return FullResponse.getInit().also {
            it.message = null
            it.image = avatar.profileImage
            it.video = avatar.muteVideo
            it.debug = null
            it.background = background.url
            it.pose = avatar.mutePose
        }
    }
}
