package ai.maum.ai_human_api.websocket.full.message.upstream

data class AudioInput(
    var audio: String
)
