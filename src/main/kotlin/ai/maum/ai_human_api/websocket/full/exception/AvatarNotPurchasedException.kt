package ai.maum.ai_human_api.websocket.full.exception

class AvatarNotPurchasedException : FullException {
    constructor(message: String) : super(message)
    constructor(cause: Throwable) : super(cause)
    constructor(message: String, cause: Throwable) : super(message, cause)
}
