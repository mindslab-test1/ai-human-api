package ai.maum.ai_human_api.websocket.full.service.talk

import ai.maum.ai_human_api.stt.Stt
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.util.stomp.MessageSender
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.full.message.downstream.FullResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class TalkStt(
    private val sessionDataFactory: ObjectFactory<SessionData>,
    private val messageSender: MessageSender,
    private val stt: Stt
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    operator fun invoke(
        audioInput: ByteArray
    ): String {
        val sessionId = sessionDataFactory.`object`.sessionId!!

        log info "Audio Size:${audioInput.size}"
        log info "Session Id:$sessionId"

        val before = Instant.now().epochSecond
        messageSender(sessionId, FullResponse.getStt("Invoked: STT").also { log info "${it.debug}" })

        val sttResult = stt.run(audioInput)
        // if no success, throw exception

        val text = sttResult.text

        messageSender(
            sessionId,
            FullResponse.getStt("STT conversion finished: $text").also {
                it.message = text
                it.duration = Instant.now().epochSecond - before
            }.also { log info "${it.debug}" }
        )

        return text
    }
}
