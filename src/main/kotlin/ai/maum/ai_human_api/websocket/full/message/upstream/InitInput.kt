package ai.maum.ai_human_api.websocket.full.message.upstream

data class InitInput(
    var projectId: Long
)
