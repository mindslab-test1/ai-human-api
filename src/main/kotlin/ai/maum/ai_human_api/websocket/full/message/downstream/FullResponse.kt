package ai.maum.ai_human_api.websocket.full.message.downstream

import java.time.Instant

data class FullResponse(
    val type: String,
    var message: String? = null,
    var image: String? = null,
    var video: String? = null,
    var background: String? = null,
    var pose: String? = null,
    var debug: String? = null,
    var expectedIntents: Array<String>? = null,
    var date: Long = Instant.now().epochSecond,
    var duration: Long = 0
) {
    companion object Companion {
        // Prototypes should return new instance every time
        fun getInit() = FullResponse(type = "INIT")
        fun getHello() = FullResponse(type = "HELLO")
        fun getKeepAlive() = FullResponse(type = "KEEPALIVE")
        fun getAnswer() = FullResponse(type = "ANSWER")

        fun getStt() = FullResponse(type = "STT")
        fun getStt(debug: String) = getStt().also { it.debug = debug }

        fun getSds() = FullResponse(type = "SDS")
        fun getSds(debug: String) = getSds().also { it.debug = debug }

        fun getAvatar() = FullResponse(type = "AVATAR")
        fun getAvatar(debug: String) = getAvatar().also { it.debug = debug }

        fun getServer() = FullResponse(type = "SERVER")
        fun getServer(debug: String) = getServer().also { it.debug = debug }

        fun getError() = FullResponse(type = "ERROR")
        fun getError(debug: String) = getError().also { it.debug = debug }
    }
}
