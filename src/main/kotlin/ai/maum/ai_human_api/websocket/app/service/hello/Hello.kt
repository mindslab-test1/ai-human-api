package ai.maum.ai_human_api.websocket.app.service.hello

import ai.maum.ai_human_api.jpa.avatar.Avatar
import ai.maum.ai_human_api.jpa.background.Background
import ai.maum.ai_human_api.jpa.scenario.Scenario
import ai.maum.ai_human_api.jpa.video.Video
import ai.maum.ai_human_api.jpa.video.VideoRepository
import ai.maum.ai_human_api.media.Media
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.util.stomp.MessageSender
import ai.maum.ai_human_api.util.text.BraceFilter
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.app.message.downstream.AppResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Service

@Service("AppHello")
class Hello(
    private val sessionDataFactory: ObjectFactory<SessionData>,
    private val messageSender: MessageSender,
    private val helloLipSyncAvatar: HelloLipSyncAvatar,
    private val helloSds: HelloSds,
    private val media: Media,
    private val videoRepository: VideoRepository
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    private val utter = "처음으로"

    operator fun invoke(): AppResponse {
        val sessionId: String = sessionDataFactory.`object`.sessionId!!
        val avatar: Avatar = sessionDataFactory.`object`.avatar!!
        val background: Background = sessionDataFactory.`object`.background!!
        val scenario: Scenario = sessionDataFactory.`object`.scenario!!

        messageSender(sessionId, AppResponse.getServer("Invoked: Hello"))

        val sdsHost = scenario.host!!

        // Run SDS
        val (answerString, expectedIntents) = helloSds(utter, sdsHost)

        val braceBlockRemovedAnswer = BraceFilter.removeTripleBraceBlock(answerString)
        val braceTransparentAnswer = BraceFilter.removeTripleBraces(answerString)

        log info "## Expected Intents: ${expectedIntents.size}, $expectedIntents"

        // Check if video is cached in media server
        val videoExists = videoRepository.findTopByAvatarAndAnswer(avatar = avatar, answer = braceBlockRemovedAnswer)
        videoExists?.let { video ->
            // if video cached, respond immediately
            return AppResponse.getHello().also {
                it.message = braceTransparentAnswer
                it.image = avatar.profileImage
                it.video = video.url
                it.background = background.url
                it.pose = avatar.talkPose
                it.expectedIntents = expectedIntents.toTypedArray()
            }
        }

        // Generate lipsync avatar
        val file = helloLipSyncAvatar(braceBlockRemovedAnswer)

        // Upload video to media server
        val uploadUrl = media.getUrl(file.filename!!)
        media.uploadToMediaServer(file, uploadUrl.pathUrl)
        val downloadUrl = media.convertDownloadUrl(uploadUrl.fullUrl)

        // Save video url to db
        run {
            val video = Video()
            video.answer = braceBlockRemovedAnswer
            video.url = downloadUrl
            video.avatar = avatar
            videoRepository.save(video)
        }

        return AppResponse.getHello().also {
            it.message = braceTransparentAnswer
            it.image = avatar.profileImage
            it.video = downloadUrl
            it.background = background.url
            it.pose = avatar.talkPose
            it.expectedIntents = expectedIntents.toTypedArray()
        }
    }
}
