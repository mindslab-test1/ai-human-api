package ai.maum.ai_human_api.websocket.app.message.upstream

data class HelloInput(
    var text: String
)
