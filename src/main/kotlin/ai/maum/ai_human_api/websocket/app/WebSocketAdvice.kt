package ai.maum.ai_human_api.websocket.app

import ai.maum.ai_human_api.jpa.video.Video
import ai.maum.ai_human_api.jpa.video.VideoRepository
import ai.maum.ai_human_api.media.Media
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.app.controller.AppController
import ai.maum.ai_human_api.websocket.app.exception.AppException
import ai.maum.ai_human_api.websocket.app.exception.AvatarNotPurchasedException
import ai.maum.ai_human_api.websocket.app.exception.SkipException
import ai.maum.ai_human_api.websocket.app.message.downstream.AppResponse
import ai.maum.ai_human_api.websocket.app.service.talk.TalkLipSyncAvatar
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.messaging.handler.annotation.MessageExceptionHandler
import org.springframework.messaging.simp.annotation.SendToUser
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.ControllerAdvice

@Component("AppWebSocketAdvice")
@ControllerAdvice(assignableTypes = [AppController::class])
class WebSocketAdvice(
    private val talkLipSyncAvatar: TalkLipSyncAvatar,
    private val media: Media,
    private val videoRepository: VideoRepository,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    @MessageExceptionHandler
    @SendToUser("/queue/human")
    fun appExceptionHandler(e: Exception): AppResponse =
        try {
            throw e
        } catch (e: SkipException) {
            AppResponse.getError()
        } catch (e: AvatarNotPurchasedException) {
            AppResponse.getError()
        } catch (e: AppException) {
            log error "${e.message}"
            AppResponse.getError()
        } catch (e: Exception) {
            log error "${e.message}"
            log error e.stackTraceToString()

            speakError("죄송합니다. 해당 내용은 제가 답변 드릴 수 없습니다.\n")
        }

    fun speakError(message: String): AppResponse =
        try {
            val avatar = sessionDataFactory.`object`.avatar!!
            val background = sessionDataFactory.`object`.background!!
            val scenario = sessionDataFactory.`object`.scenario!!

            val sdsHost = scenario.host!!
            // Check if video is cached in media server
            val videoExists = videoRepository.findTopByAvatarAndAnswer(avatar = avatar, answer = message)
            videoExists?.let { video ->
                // if video cached, respond immediately
                return AppResponse.getError().also {
                    it.message = message
                    it.image = avatar.profileImage
                    it.video = video.url
                    it.background = background.url
                    it.pose = avatar.talkPose
                    it.expectedIntents = null
                }
            }

            // Generate lipsync avatar
            val file = talkLipSyncAvatar(message)

            // Upload video to media server
            val uploadUrl = media.getUrl(file.filename!!)

            media.uploadToMediaServer(file, uploadUrl.pathUrl)

            val downloadUrl = media.convertDownloadUrl(uploadUrl.fullUrl)

            // Save video url to db
            run {
                val video = Video()
                video.answer = message
                video.url = downloadUrl
                video.avatar = avatar
                videoRepository.save(video)
            }

            AppResponse.getError().also {
                it.message = message
                it.image = avatar.profileImage
                it.video = downloadUrl
                it.background = background.url
                it.pose = avatar.talkPose
                it.expectedIntents = null
            }
        } catch (e: Exception) {
            AppResponse.getError().also {
                it.message = "알 수 없는 오류"
            }
        }
}
