package ai.maum.ai_human_api.websocket.app.message.upstream

data class KeepAliveInput(
    var text: String
)
