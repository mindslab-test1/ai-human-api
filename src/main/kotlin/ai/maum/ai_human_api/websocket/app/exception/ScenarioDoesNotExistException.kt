package ai.maum.ai_human_api.websocket.app.exception

class ScenarioDoesNotExistException : AppException {
    constructor(message: String) : super(message)
    constructor(cause: Throwable) : super(cause)
    constructor(message: String, cause: Throwable) : super(message, cause)
}
