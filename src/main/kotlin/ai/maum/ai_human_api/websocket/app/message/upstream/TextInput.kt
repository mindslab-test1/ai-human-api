package ai.maum.ai_human_api.websocket.app.message.upstream

data class TextInput(
    var text: String
)
