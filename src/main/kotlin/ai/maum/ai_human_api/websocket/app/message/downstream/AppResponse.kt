package ai.maum.ai_human_api.websocket.app.message.downstream

import java.time.Instant

data class AppResponse(
    val type: String,
    var message: String? = null,
    var image: String? = null,
    var video: String? = null,
    var background: String? = null,
    var pose: String? = null,
    var debug: String? = null,
    var expectedIntents: Array<String>? = null,
    var date: Long = Instant.now().epochSecond,
    var duration: Long = 0
) {
    data class AutoCompleteJson(
        var text: String? = null,
        var score: Double? = null
    )

    companion object Companion {
        // Prototypes should return new instance every time
        fun getInit() = AppResponse(type = "INIT")
        fun getHello() = AppResponse(type = "HELLO")
        fun getKeepAlive() = AppResponse(type = "KEEPALIVE")
        fun getAnswer() = AppResponse(type = "ANSWER")

        fun getStt() = AppResponse(type = "STT")
        fun getStt(debug: String) = getStt().also { it.debug = debug }

        fun getSds() = AppResponse(type = "SDS")
        fun getSds(debug: String) = getSds().also { it.debug = debug }

        fun getAvatar() = AppResponse(type = "AVATAR")
        fun getAvatar(debug: String) = getAvatar().also { it.debug = debug }

        fun getServer() = AppResponse(type = "SERVER")
        fun getServer(debug: String) = getServer().also { it.debug = debug }

        fun getError() = AppResponse(type = "ERROR")
        fun getError(debug: String) = getError().also { it.debug = debug }
    }
}
