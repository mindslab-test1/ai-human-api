package ai.maum.ai_human_api.websocket.poc.message

data class RawPocMeta(
    var type: String? = null,
    var message: String? = null,
    var url: String? = null
)
