package ai.maum.ai_human_api.websocket.poc.message.upstream

data class AudioInput(
    var audio: String
)
