package ai.maum.ai_human_api.websocket.poc.message.upstream

data class KeepAliveInput(
    var text: String
)
