package ai.maum.ai_human_api.websocket.poc.message.upstream

data class BabbleInput(
    var pcmBytes: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BabbleInput

        if (!pcmBytes.contentEquals(other.pcmBytes)) return false

        return true
    }

    override fun hashCode(): Int {
        return pcmBytes.contentHashCode()
    }
}
