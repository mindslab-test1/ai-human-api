package ai.maum.ai_human_api.websocket.poc.service.babble

import ai.maum.ai_human_api.grpc.stt.SttGrpcClient
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.util.stomp.MessageSender
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.poc.message.downstream.PocResponse
import ai.maum.ai_human_api.websocket.poc.message.upstream.BabbleInput
import com.google.protobuf.ByteString
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Service

@Service("PocBabble")
class Babble(
    private val sessionDataFactory: ObjectFactory<SessionData>,
    private val messageSender: MessageSender,
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    operator fun invoke(babbleInput: BabbleInput): PocResponse {
        sessionDataFactory.`object`.sttGrpcClient!!.onNext(
            SttGrpcClient.newSpeech(ByteString.copyFrom(babbleInput.pcmBytes))
        )

        // actual response will be sent by responseObserver
        // always return empty response
        return PocResponse.getBabble()
    }
}
