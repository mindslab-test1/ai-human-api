package ai.maum.ai_human_api.websocket.poc.exception

class SkipException : PocException {
    constructor(message: String) : super(message)
    constructor(cause: Throwable) : super(cause)
    constructor(message: String, cause: Throwable) : super(message, cause)
}
