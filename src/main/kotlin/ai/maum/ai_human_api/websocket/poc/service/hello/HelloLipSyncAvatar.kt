package ai.maum.ai_human_api.websocket.poc.service.hello

import ai.maum.ai_human_api.lipsync.LipSync
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.util.stomp.MessageSender
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.poc.exception.LipSyncAvatarFailedException
import ai.maum.ai_human_api.websocket.poc.message.downstream.PocResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service

@Service("PocHelloLipSyncAvatar")
class HelloLipSyncAvatar(
    private val sessionDataFactory: ObjectFactory<SessionData>,
    private val messageSender: MessageSender,
    private val lipSync: LipSync
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    private val _processing = 1L
    private val _finished = 2L
    private val _sleepInterval = 200L

    operator fun invoke(
        answerString: String
    ): Resource {
        val sessionId: String = sessionDataFactory.`object`.sessionId!!

        messageSender(sessionId, PocResponse.getServer("Invoked: LipSync Avatar").also { log info "${it.debug}" })

        val lipSyncUploadResult = lipSync.runUpload(answerString)
        val requestKey = lipSyncUploadResult.requestKey

        var prevWaiting = 0L
        var lastStatusCode = 0L
        while (true) {
            val lipSyncCheckResult = lipSync.runCheck(requestKey)
            val statusCode = lipSyncCheckResult.statusCode
            val waiting = lipSyncCheckResult.waiting

            if (waiting >= 0 && waiting != prevWaiting) {
                prevWaiting = waiting
                messageSender(sessionId, PocResponse.getAvatar("Waiting Queue: size $waiting").also { log info "${it.debug}" })
            }

            if (statusCode == _processing) {
                lastStatusCode = _processing
                break // 처리중
            }
            if (statusCode == _finished) {
                lastStatusCode = _finished
                break // 완료됨
            }
            if (statusCode > _finished)
                throw LipSyncAvatarFailedException("LipSync Avatar failed 1; status code: $statusCode")

            Thread.sleep(_sleepInterval)
        }

        messageSender(sessionId, PocResponse.getAvatar("Avatar is being rendered...").also { log info "${it.debug}" })

        while (lastStatusCode != _finished) {
            val lipSyncCheckResult = lipSync.runCheck(requestKey)
            lastStatusCode = lipSyncCheckResult.statusCode
            if (lastStatusCode > _finished)
                throw LipSyncAvatarFailedException("LipSync Avatar failed 2; status code: $lastStatusCode")

            Thread.sleep(_sleepInterval)
        }

        return lipSync.runDownload(requestKey)
    }
}
