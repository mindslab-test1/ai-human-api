package ai.maum.ai_human_api.websocket.poc.message.downstream

import java.time.Instant

data class PocResponse(
    val type: String,
    var message: String? = null,
    var image: String? = null,
    var video: String? = null,
    var background: String? = null,
    var pose: String? = null,
    var debug: String? = null,
    var babble: Word? = null,
    var expectedIntents: Array<String>? = null,
    var pocMeta: PocMeta? = null,
    var date: Long = Instant.now().epochSecond,
    var duration: Long = 0
) {
    data class Word(
        val start: Int,
        val end: Int,
        val txt: String
    )

    data class PocMeta(
        var type: String? = null,
        var image: String? = null
    )

    companion object Companion {
        // Prototypes should return new instance every time
        fun getInit() = PocResponse(type = "INIT")
        fun getHello() = PocResponse(type = "HELLO")
        fun getKeepAlive() = PocResponse(type = "KEEPALIVE")
        fun getAnswer() = PocResponse(type = "ANSWER")

        fun getStt() = PocResponse(type = "STT")
        fun getStt(debug: String) = getStt().also { it.debug = debug }

        fun getSds() = PocResponse(type = "SDS")
        fun getSds(debug: String) = getSds().also { it.debug = debug }

        fun getAvatar() = PocResponse(type = "AVATAR")
        fun getAvatar(debug: String) = getAvatar().also { it.debug = debug }

        fun getBabble() = PocResponse(type = "BABBLE")
        fun getBabble(babble: Word) = getBabble().also { it.babble = babble }

        fun getServer() = PocResponse(type = "SERVER")
        fun getServer(debug: String) = getServer().also { it.debug = debug }

        fun getError() = PocResponse(type = "ERROR")
        fun getError(debug: String) = getError().also { it.debug = debug }

        fun getFdNot() = PocResponse(type = "FD_NOT")
        fun getFdFar() = PocResponse(type = "FD_FAR")
        fun getFdNear() = PocResponse(type = "FD_NEAR")
    }
}
