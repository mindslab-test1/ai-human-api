package ai.maum.ai_human_api.websocket.poc.controller

import ai.maum.ai_human_api.jpa.talklog.TalkLog
import ai.maum.ai_human_api.jpa.talklog.TalkLogRepository
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.poc.message.downstream.PocResponse
import ai.maum.ai_human_api.websocket.poc.message.upstream.*
import ai.maum.ai_human_api.websocket.poc.service.babble.Babble
import ai.maum.ai_human_api.websocket.poc.service.babble.BabbleBabble
import ai.maum.ai_human_api.websocket.poc.service.hello.Hello
import ai.maum.ai_human_api.websocket.poc.service.init.Init
import ai.maum.ai_human_api.websocket.poc.service.talk.Talk
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.messaging.simp.annotation.SendToUser
import org.springframework.stereotype.Controller
import java.util.*

@MessageMapping("/poc")
@Controller
class PocController(
    private val init: Init,
    private val hello: Hello,
    private val talk: Talk,
    private val babble: Babble,
    private val babbleBabble: BabbleBabble,
    private val sessionDataFactory: ObjectFactory<SessionData>,
    private val talkLogRepository: TalkLogRepository
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    /**
     * After STOMP WebSocket connection established, client should send INIT message first.
     * Init validates projectId and responds mute video information.
     *
     * @param message configured projectId of client.
     * @param sessionId WebSocket session ID.
     * @return IntegratedResponse containing mute video.
     */
    @MessageMapping("/init")
    @SendToUser("/queue/human")
    fun handleInit(@Payload message: InitInput, @Header("simpSessionId") sessionId: String): PocResponse {
        log info "Init"
        return init(message, sessionId)
    }

    /**
     * Client should send HELLO message after initialization.
     * Hello sends "처음으로" utter to SDS and receives greeting message.
     *
     * @param message placeholder.
     * @param sessionId WebSocket session ID.
     * @return IntegratedResponse containing greeting message and video.
     */
    @MessageMapping("/hello")
    @SendToUser("/queue/human")
    fun handleHello(@Payload message: HelloInput, @Header("simpSessionId") sessionId: String): PocResponse {
        log info "Hello"
        return hello()
    }

    /**
     * Keeps STOMP WebSocket connection alive.
     *
     * @param message placeholder.
     * @param sessionId WebSocket session ID.
     * @return empty IntegratedResponse.
     */
    @MessageMapping("/keepalive")
    @SendToUser("/queue/human")
    fun handleKeepAlive(@Payload message: String, @Header("simpSessionId") sessionId: String): PocResponse {
        log info "Keep-alive"
        return PocResponse.getKeepAlive()
    }

    /**
     * Sends pcm bytes to STT engine via gRPC
     * STT results will be received by responseObserver in SessionData.
     *
     * @param message wave pcm bytes.
     * @param sessionid WebSocket session ID.
     * @return empty IntegratedResponse.
     */
    @MessageMapping("/babble")
    @SendToUser("/queue/human")
    fun handleBabble(@Payload message: BabbleInput, @Header("simpSessionId") sessionId: String): PocResponse {
        // log info "Babble: (${message.pcmBytes.size}) ${Base64.getMimeEncoder().encodeToString(message.pcmBytes).substring(0, 24)}"
        return babble(message)
    }

    /**
     * Sends pcm bytes to STT engine via gRPC
     * STT results will be received by responseObserver in SessionData.
     *
     * @param message wave pcm bytes.
     * @param sessionid WebSocket session ID.
     * @return empty IntegratedResponse.
     */
    @MessageMapping("/babblebabble")
    @SendToUser("/queue/human")
    fun handleBabbleBabble(@Payload message: BabbleBabbleInput, @Header("simpSessionId") sessionId: String): PocResponse {
        // log info "Babble: (${message.pcmBytes.size}) ${Base64.getMimeEncoder().encodeToString(message.pcmBytes).substring(0, 24)}"
        return babbleBabble(message)
    }

    /**
     * Talk with audio(.wav) input.
     *
     * @param message base64 string of input audio(.wav) file.
     * @return IntegratedResponse answer.
     */
    @MessageMapping("/audioinput")
    @SendToUser("/queue/human")
    fun handleAudioInput(@Payload message: AudioInput): PocResponse? = withTalkLogging {
        return@withTalkLogging talk(Base64.getMimeDecoder().decode(message.audio), null)
    }

    /**
     * Talk with text input.
     *
     * @param message input text string.
     * @return IntegratedResponse answer.
     */
    @MessageMapping("/textinput")
    @SendToUser("/queue/human")
    fun handleTextInput(@Payload message: TextInput): PocResponse = withTalkLogging {
        return@withTalkLogging talk(null, message)
    }

    // Handle message logging only; exceptions are handled in WebSocketAdvice
    private fun withTalkLogging(handler: PocController.() -> PocResponse): PocResponse =
        try {
            sessionDataFactory.`object`.talkLog = TalkLog()
            handler()
        } catch (e: Exception) {
            // Delegate exceptions to @ControllerAdvice
            throw e
        } finally {
            talkLogRepository.save(sessionDataFactory.`object`.talkLog!!)
        }
}
