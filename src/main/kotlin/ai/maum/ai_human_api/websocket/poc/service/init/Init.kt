package ai.maum.ai_human_api.websocket.poc.service.init

import ai.maum.ai_human_api.builder.flow.Flow
import ai.maum.ai_human_api.builder.license.License
import ai.maum.ai_human_api.grpc.stt.SttGrpcClient
import ai.maum.ai_human_api.jpa.avatar.AvatarRepository
import ai.maum.ai_human_api.jpa.background.BackgroundRepository
import ai.maum.ai_human_api.jpa.scenario.Scenario
import ai.maum.ai_human_api.jpa.scenario.ScenarioRepository
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.util.stomp.MessageSender
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.poc.exception.AvatarDoesNotExistException
import ai.maum.ai_human_api.websocket.poc.exception.BackgroundDoesNotExistException
import ai.maum.ai_human_api.websocket.poc.exception.InvalidLicenseException
import ai.maum.ai_human_api.websocket.poc.exception.ScenarioDoesNotExistException
import ai.maum.ai_human_api.websocket.poc.message.downstream.PocResponse
import ai.maum.ai_human_api.websocket.poc.message.upstream.InitInput
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service("PocInit")
class Init(
    private val flow: Flow,
    private val license: License,
    private val messageSender: MessageSender,
    private val avatarRepository: AvatarRepository,
    private val backgroundRepository: BackgroundRepository,
    private val scenarioRepository: ScenarioRepository,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    private val specialProjectId = arrayOf(-100L, -200L, -300L, -400L, -500L, -2000L, -2100L, -5000L)
    private val pocProjectId = arrayOf(-5000L)

    operator fun invoke(message: InitInput, sessionId: String): PocResponse {
        log info "# Processing init message(poc)"

        sessionDataFactory.`object`.sessionId = sessionId
        sessionDataFactory.`object`.flowId = message.projectId
        sessionDataFactory.`object`.sttGrpcClient = SttGrpcClient(sessionId, messageSender)

        val flowData = flow.getFlowInfo(message.projectId)

        // Human profile check
        var avatar = avatarRepository.findTopByModel(flowData.avatar) ?: throw AvatarDoesNotExistException("Avatar ${flowData.avatar} does not exist")
        val background = backgroundRepository.findByIdOrNull(flowData.background) ?: throw BackgroundDoesNotExistException("Background ${flowData.background} does not exist")
        var scenarioExists = scenarioRepository.findTopByHost(flowData.scenario)
        if (scenarioExists == null) {
            scenarioExists = Scenario()
            scenarioExists.id = null
            scenarioExists.host = flowData.scenario
            scenarioRepository.save(scenarioExists)
        }
        val scenario = scenarioRepository.findTopByHost(flowData.scenario) ?: throw ScenarioDoesNotExistException("Scenario ${flowData.scenario} does not exist")

        if (message.projectId == -5000L) {
            avatar = avatarRepository.findByIdOrNull(10000) ?: throw AvatarDoesNotExistException("10000 Avatar does not exist")
        }

        sessionDataFactory.`object`.avatar = avatar
        sessionDataFactory.`object`.background = background
        sessionDataFactory.`object`.scenario = scenario

        log info
            "\nAvatar: ${flowData.avatar} -> ${avatar.id}: ${avatar.model}" +
            "\nScenario: ${flowData.scenario} -> ${scenario.id}: ${scenario.host}" +
            "\nBackground: ${flowData.background} -> ${background.id}: ${background.name}" +
            "\nMuteVideo: ${avatar.muteVideo}" +
            "\nProfileImage: ${avatar.profileImage}" +
            "\nPose: ${avatar.mutePose}"

        if (!specialProjectId.contains(message.projectId))
            if (!license.validateLicense(flowData.email).valid)
                throw InvalidLicenseException("User ${flowData.email} with project ID ${message.projectId} does not have valid license")

        return PocResponse.getInit().also {
            it.message = null
            it.image = avatar.profileImage
            it.video = avatar.muteVideo
            it.debug = null
            it.background = background.url
            it.pose = avatar.mutePose
        }.also {
            if (pocProjectId.contains(message.projectId))
                it.video = "https://media.maum.ai/media/service-dev/ai-human/display-prod/mute/mute_poc.mp4"
        }
    }
}
