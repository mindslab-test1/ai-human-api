package ai.maum.ai_human_api.websocket.poc.message.upstream

data class InitInput(
    var projectId: Long
)
