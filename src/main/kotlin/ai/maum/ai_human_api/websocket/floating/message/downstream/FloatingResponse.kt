package ai.maum.ai_human_api.websocket.floating.message.downstream

import java.time.Instant

data class FloatingResponse(
    val type: String,
    var message: String? = null,
    var image: String? = null,
    var video: String? = null,
    var background: String? = null,
    var pose: String? = null,
    var debug: String? = null,
    var expectedIntents: Array<String>? = null,
    var autoComplete: Array<AutoCompleteJson>? = null,
    var date: Long = Instant.now().epochSecond,
    var duration: Long = 0
) {
    data class AutoCompleteJson(
        var text: String? = null,
        var score: Double? = null
    )

    companion object Companion {
        // Prototypes should return new instance every time
        fun getInit() = FloatingResponse(type = "INIT")
        fun getHello() = FloatingResponse(type = "HELLO")
        fun getKeepAlive() = FloatingResponse(type = "KEEPALIVE")
        fun getAutoComplete() = FloatingResponse(type = "AUTOCOMPLETE")
        fun getAnswer() = FloatingResponse(type = "ANSWER")

        fun getStt() = FloatingResponse(type = "STT")
        fun getStt(debug: String) = getStt().also { it.debug = debug }

        fun getSds() = FloatingResponse(type = "SDS")
        fun getSds(debug: String) = getSds().also { it.debug = debug }

        fun getAvatar() = FloatingResponse(type = "AVATAR")
        fun getAvatar(debug: String) = getAvatar().also { it.debug = debug }

        fun getServer() = FloatingResponse(type = "SERVER")
        fun getServer(debug: String) = getServer().also { it.debug = debug }

        fun getError() = FloatingResponse(type = "ERROR")
        fun getError(debug: String) = getError().also { it.debug = debug }
    }
}
