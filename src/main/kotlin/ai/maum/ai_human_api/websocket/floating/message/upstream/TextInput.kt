package ai.maum.ai_human_api.websocket.floating.message.upstream

data class TextInput(
    var text: String
)
