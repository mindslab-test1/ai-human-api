package ai.maum.ai_human_api.websocket.floating

import ai.maum.ai_human_api.jpa.video.Video
import ai.maum.ai_human_api.jpa.video.VideoRepository
import ai.maum.ai_human_api.media.Media
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.floating.controller.FloatingController
import ai.maum.ai_human_api.websocket.floating.exception.AvatarNotPurchasedException
import ai.maum.ai_human_api.websocket.floating.exception.FloatingException
import ai.maum.ai_human_api.websocket.floating.exception.SkipException
import ai.maum.ai_human_api.websocket.floating.message.downstream.FloatingResponse
import ai.maum.ai_human_api.websocket.floating.service.talk.TalkLipSyncAvatar
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.messaging.handler.annotation.MessageExceptionHandler
import org.springframework.messaging.simp.annotation.SendToUser
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.ControllerAdvice

@Component("FloatingWebSocketAdvice")
@ControllerAdvice(assignableTypes = [FloatingController::class])
class WebSocketAdvice(
    private val talkLipSyncAvatar: TalkLipSyncAvatar,
    private val media: Media,
    private val videoRepository: VideoRepository,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    @MessageExceptionHandler
    @SendToUser("/queue/human")
    fun floatingExceptionHandler(e: Exception): FloatingResponse =
        try {
            throw e
        } catch (e: SkipException) {
            FloatingResponse.getError()
        } catch (e: AvatarNotPurchasedException) {
            FloatingResponse.getError()
        } catch (e: FloatingException) {
            log error "${e.message}"
            FloatingResponse.getError()
        } catch (e: Exception) {
            log error "${e.message}"
            log error e.stackTraceToString()

            speakError("죄송합니다. 해당 내용은 제가 답변 드릴 수 없습니다.\n")
        }

    fun speakError(message: String): FloatingResponse =
        try {
            val avatar = sessionDataFactory.`object`.avatar!!
            val background = sessionDataFactory.`object`.background!!
            val scenario = sessionDataFactory.`object`.scenario!!

            val sdsHost = scenario.host!!
            // Check if video is cached in media server
            val videoExists = videoRepository.findTopByAvatarAndAnswer(avatar = avatar, answer = message)
            videoExists?.let { video ->
                // if video cached, respond immediately
                return FloatingResponse.getError().also {
                    it.message = message
                    it.image = avatar.profileImage
                    it.video = video.url
                    it.background = background.url
                    it.pose = avatar.talkPose
                    it.expectedIntents = null
                }
            }

            // Generate lipsync avatar
            val file = talkLipSyncAvatar(message)

            // Upload video to media server
            val uploadUrl = media.getUrl(file.filename!!)

            media.uploadToMediaServer(file, uploadUrl.pathUrl)

            val downloadUrl = media.convertDownloadUrl(uploadUrl.fullUrl)

            // Save video url to db
            run {
                val video = Video()
                video.answer = message
                video.url = downloadUrl
                video.avatar = avatar
                videoRepository.save(video)
            }

            FloatingResponse.getError().also {
                it.message = message
                it.image = avatar.profileImage
                it.video = downloadUrl
                it.background = background.url
                it.pose = avatar.talkPose
                it.expectedIntents = null
            }
        } catch (e: Exception) {
            FloatingResponse.getError().also {
                it.message = "알 수 없는 오류"
            }
        }

    /*
            val floatingPayload = FloatingResponse.getError(e.message)
        val floatingPayloadLength = floatingPayload.toString().length

        return object : WebSocketMessage<FloatingResponse> {
            override fun isLast(): Boolean {
                return true
            }
            override fun getPayload(): FloatingResponse {
                return floatingPayload
            }
            override fun getPayloadLength(): Int {
                return floatingPayloadLength
            }
        }
     */
}
