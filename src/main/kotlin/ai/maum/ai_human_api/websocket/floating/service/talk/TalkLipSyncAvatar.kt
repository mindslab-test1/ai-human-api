package ai.maum.ai_human_api.websocket.floating.service.talk

import ai.maum.ai_human_api.lipsync.LipSync
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.util.stomp.MessageSender
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.floating.exception.LipSyncAvatarFailedException
import ai.maum.ai_human_api.websocket.floating.message.downstream.FloatingResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service

@Service("FloatingTalkKipSyncAvatar")
class TalkLipSyncAvatar(
    private val sessionDataFactory: ObjectFactory<SessionData>,
    private val messageSender: MessageSender,
    private val lipSync: LipSync
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    private val _processing = 1L
    private val _finished = 2L
    private val _processError = 4L

    private val _sleepInterval = 200L

    operator fun invoke(
        answerString: String
    ): Resource {
        val sessionId: String = sessionDataFactory.`object`.sessionId!!

        messageSender(sessionId, FloatingResponse.getServer("Invoked: LipSync Avatar").also { log info "${it.debug}" })

        val lipSyncUploadResult = lipSync.runUpload(answerString)
        val requestKey = lipSyncUploadResult.requestKey

        var prevWaiting = 0L
        var lastStatusCode = 0L
        while (true) {
            val lipSyncCheckResult = lipSync.runCheck(requestKey)
            val statusCode = lipSyncCheckResult.statusCode
            val waiting = lipSyncCheckResult.waiting

            if (waiting >= 0 && waiting != prevWaiting) {
                prevWaiting = waiting
                messageSender(sessionId, FloatingResponse.getAvatar("Waiting Queue: size $waiting").also { log info "${it.debug}" })
            }

            if (statusCode == _processing) {
                lastStatusCode = _processing
                break // 처리중
            }
            if (statusCode == _finished) {
                lastStatusCode = _finished
                break // 완료됨
            }
            if (statusCode > _finished)
                throw LipSyncAvatarFailedException("LipSync Avatar failed 1; status code: $statusCode")

            Thread.sleep(_sleepInterval)
        }

        messageSender(sessionId, FloatingResponse.getAvatar("Avatar is being rendered...").also { log info "${it.debug}" })

        while (lastStatusCode != _finished) {
            val lipSyncCheckResult = lipSync.runCheck(requestKey)
            lastStatusCode = lipSyncCheckResult.statusCode
            if (lastStatusCode > _finished)
                throw LipSyncAvatarFailedException("LipSync Avatar failed 2; status code: $lastStatusCode")

            Thread.sleep(_sleepInterval)
        }

        return lipSync.runDownload(requestKey)
    }
}
