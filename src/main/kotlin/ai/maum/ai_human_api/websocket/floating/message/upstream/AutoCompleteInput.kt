package ai.maum.ai_human_api.websocket.floating.message.upstream

data class AutoCompleteInput(
    var text: String
)
