package ai.maum.ai_human_api.websocket.floating.service.talk

import ai.maum.ai_human_api.sds.* // ktlint-disable no-wildcard-imports
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.util.stomp.MessageSender
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.floating.message.downstream.FloatingResponse
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Service
import java.time.Instant
import kotlin.math.min

@Service("FloatingTalkSds")
class TalkSds(
    private val sessionDataFactory: ObjectFactory<SessionData>,
    private val messageSender: MessageSender,
    private val sds: Sds
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    operator fun invoke(
        textInput: String,
        host: Long
    ): Pair<String, List<String>> {
        val sessionId = sessionDataFactory.`object`.sessionId!!

        val before = Instant.now().epochSecond
        messageSender(sessionId, FloatingResponse.getSds("Invoked: Sds").also { log info "${it.debug}" })

        val sdsResult: SdsRunResponse = sds.run(textInput, host, sessionId)
        log info "Answer: ${sdsResult.answer?.answer}"

        var engine = ""
        var prob = ""
        var answer = ""
        var intent = sdsResult.setMemory?.intent?.intent ?: ""

        var regex = "pass"
        var nqa = "pass"
        var bert = "pass"
        var kbqa = "pass"
        var newsMrc = "pass"
        var wikiMrc = "pass"

        /*
            "KBQA"
            "NEWS_MRC"
            "WIKI_MRC"
         */
        var throughput = ""
        if (sdsResult.setMemory?.intent?.intent != "죄송") {
            val debugJson = jacksonObjectMapper().readValue<SdsDebugResponse.DebugJson>(sdsResult.jsonDebug!!)

            engine = debugJson.engine!!
            when (debugJson.engine) {
                "RegEx" -> regex = debugJson.model ?: "ERROR"
                "NQA" -> nqa = sdsResult.setMemory?.intent?.intent ?: "ERROR"
                "BERT" -> bert = sdsResult.setMemory?.intent?.intent ?: "ERROR"
            }

            prob = debugJson.prob!!.toString()
            answer = sdsResult.answer!!.answer!!
        } else {
            val restApiDto = sdsResult.meta!!.restApiDto!!
            var mrc = true
            var fallback = false
            when (restApiDto.engine) {
                "KBQA" -> {
                    kbqa = "KBQA 답변"
                    mrc = false
                    prob = restApiDto.probability.toString()
                    if (restApiDto.probability!! <= 0.000001)
                        fallback = true
                }
                "NEWS_MRC" -> newsMrc = "MRC 답변"
                "WIKI_MRC" -> wikiMrc = "MRC 답변"
            }
            engine = restApiDto.engine!!
            answer = sdsResult.answer!!.answer!!

            intent = engine

            if (mrc) {
                restApiDto.meta?.let {
                    val meta = jacksonObjectMapper().readValue<Array<MrcMeta>>(restApiDto.meta!!)
                    val maxlen = min(meta.size, 2)
                    for (index in 0 until maxlen) {
                        val element = meta[index]
                        throughput += "MRC\n" +
                            "Answer : $answer\n" +
                            "[$engine]\n" +
                            "\n" +
                            "Log : \n" +
                            "[$engine] [${element.title}] ${element.sentence} \n" +
                            "answerRank : ${index + 1} \n" +
                            "answerProb : ${element.answerProb} \n" +
                            "frequency :  ${element.frequency} \n" +
                            "searchProb : ${element.searchProb} \n"
                        if (index < 2)
                            throughput += "\n"
                    }

                    prob = meta.getOrNull(0)?.answerProb?.toString() ?: ""
                    if (prob == "")
                        fallback = true
                }
            } else {
                restApiDto.meta?.let {
                    val meta = jacksonObjectMapper().readValue<KbqaMeta>(restApiDto.meta!!).map!!
                    throughput += "KBQA\n" +
                        "Answer : $answer\n" +
                        "[$engine]\n" +
                        "\n" +
                        "Log : \n" +
                        "[$engine] [${meta.subject}] [${meta.property}] \n" +
                        "answerRank : 1\n" +
                        "confidence : 1.0\n"
                }
            }

            if (fallback) {
                newsMrc = "pass"
                wikiMrc = "pass"
                engine = "Fallback"

                intent = "Fallback"
            }
        }

        messageSender(
            sessionId,
            FloatingResponse.getSds(
                "의도: $intent\n" +
                    "엔진: $engine\n" +
                    "- Regex: $regex\n" +
                    "- NQA: $nqa\n" +
                    "- BERT: $bert\n" +
                    "- KBQA: $kbqa\n" +
                    "- NEWS_MRC: $newsMrc\n" +
                    "- WIKI_MRC: $wikiMrc\n" +
                    "- Prob: $prob\n" +
                    "답변: \n" +
                    answer
            ).also {
                it.date - before
            }.also {
                log info "${it.debug}"
            }
        )
        messageSender.takeIf { throughput.isNotEmpty() }?.invoke(
            sessionId,
            FloatingResponse.getSds(throughput).also {
                log info "${it.debug}"
            }
        )

        return Pair(sdsResult.answer?.answer!!, sdsResult.expectedIntents?.mapNotNull { it.displayName } ?: listOf())
    }
}
