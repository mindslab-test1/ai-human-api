package ai.maum.ai_human_api.websocket.floating.message.upstream

data class HelloInput(
    var text: String
)
