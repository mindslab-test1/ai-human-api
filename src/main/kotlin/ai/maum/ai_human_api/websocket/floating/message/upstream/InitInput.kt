package ai.maum.ai_human_api.websocket.floating.message.upstream

data class InitInput(
    var projectId: Long
)
