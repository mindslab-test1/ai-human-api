package ai.maum.ai_human_api.websocket.floating.service.talk

import ai.maum.ai_human_api.jpa.avatar.Avatar
import ai.maum.ai_human_api.jpa.background.Background
import ai.maum.ai_human_api.jpa.scenario.Scenario
import ai.maum.ai_human_api.jpa.talklog.TalkLog
import ai.maum.ai_human_api.jpa.video.Video
import ai.maum.ai_human_api.jpa.video.VideoRepository
import ai.maum.ai_human_api.media.Media
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.util.stomp.MessageSender
import ai.maum.ai_human_api.util.text.BraceFilter
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.floating.exception.InvalidInputException
import ai.maum.ai_human_api.websocket.floating.exception.SkipException
import ai.maum.ai_human_api.websocket.floating.message.downstream.FloatingResponse
import ai.maum.ai_human_api.websocket.floating.message.upstream.TextInput
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Service
import java.time.Instant

@Service("FloatingTalk")
class Talk(
    private val sessionDataFactory: ObjectFactory<SessionData>,
    private val messageSender: MessageSender,
    private val talkStt: TalkStt,
    private val talkSds: TalkSds,
    private val talkLipSyncAvatar: TalkLipSyncAvatar,
    private val media: Media,
    private val videoRepository: VideoRepository
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    operator fun invoke(
        audioInput: ByteArray? = null,
        textInput: TextInput? = null
    ): FloatingResponse {
        val sessionId: String = sessionDataFactory.`object`.sessionId!!
        val avatar: Avatar = sessionDataFactory.`object`.avatar!!
        val scenario: Scenario = sessionDataFactory.`object`.scenario!!
        val background: Background = sessionDataFactory.`object`.background!!
        val talkLog: TalkLog = sessionDataFactory.`object`.talkLog!!

        messageSender(sessionId, FloatingResponse.getServer("Invoked: Talk"))

        val sdsHost = scenario.host!!

        talkLog.sessionId = sessionDataFactory.`object`.sessionId
        talkLog.flowId = sessionDataFactory.`object`.flowId
        talkLog.avatarModel = avatar.model
        talkLog.backgroundName = background.name
        talkLog.scenarioHost = scenario.host
        talkLog.lastProc = "TALK_START"

        // Asserting only one nonnull input
        audioInput ?: textInput ?: throw InvalidInputException("AudioInput and TextInput both null")
        audioInput?.let {
            textInput?.let {
                throw InvalidInputException("AudioInput and TextInput both NOT null")
            }
        }

        // Input logging
        audioInput?.let {
            log info "Audio input received"
        }
        textInput?.let {
            log info "Text input received"
            log info "Text: ${textInput.text}"
        }

        // Run STT
        var userText = ""

        audioInput?.let {
            talkLog.lastProc = "TALK_BEFORE_STT"
            userText = talkStt(audioInput)
            talkLog.lastProc = "TALK_AFTER_STT"
        }
        textInput?.let {
            userText = textInput.text
        }

        talkLog.inputText = userText
        talkLog.lastProc = "TALK_BEFORE_SKIPCHECK"

        if (userText.isEmpty())
            throw SkipException("skip")

        talkLog.lastProc = "TALK_BEFORE_SDS"

        // Run SDS
        val (answerString, expectedIntents) = talkSds(userText, sdsHost)

        talkLog.outputText = answerString
        talkLog.lastProc = "TALK_AFTER_SDS"

        val braceBlockRemovedAnswer = BraceFilter.removeTripleBraceBlock(answerString)
        val braceTransparentAnswer = BraceFilter.removeTripleBraces(answerString)

        // Check if video is cached in media server
        val videoExists = videoRepository.findTopByAvatarAndAnswer(avatar = avatar, answer = braceBlockRemovedAnswer)
        videoExists?.let { video ->
            // if video cached, respond immediately
            messageSender(
                sessionId,
                FloatingResponse.getServer("Video already exists in cache").also {
                    talkLog.videoUrl = video.url
                    talkLog.lastProc = "TALK_VIDEO_CACHED"
                }
            )
            return FloatingResponse.getAnswer().also {
                it.message = answerString
                it.image = avatar.profileImage
                it.video = video.url
                it.background = background.url
                it.pose = avatar.talkPose
                it.expectedIntents = expectedIntents.toTypedArray()
            }
        }
        talkLog.lastProc = "TALK_BEFORE_LIPSYNC"

        // Generate lipsync avatar
        val avatarBefore = Instant.now().epochSecond
        val file = talkLipSyncAvatar(braceBlockRemovedAnswer)

        talkLog.lastProc = "TALK_BEFORE_MEDIA_GETURL"

        // Upload video to media server
        val uploadUrl = media.getUrl(file.filename!!)

        talkLog.lastProc = "TALK_BEFORE_MEDIA_UPLOAD"
        media.uploadToMediaServer(file, uploadUrl.pathUrl)

        talkLog.lastProc = "TALK_BEFORE_MEDIA_DOWNLOADURL"
        val downloadUrl = media.convertDownloadUrl(uploadUrl.fullUrl)

        talkLog.videoUrl = downloadUrl
        talkLog.lastProc = "TALK_BEFORE_VIDEO_SAVE"

        // Save video url to db
        run {
            val video = Video()
            video.answer = braceBlockRemovedAnswer
            video.url = downloadUrl
            video.avatar = avatar
            videoRepository.save(video)
        }

        talkLog.lastProc = "TALK_BEFORE_RETURN_ANSWER"

        // Send "finished generating avatar" debug message
        messageSender(
            sessionId,
            FloatingResponse.getAvatar("Avatar generated successfully")
                .also { it.duration = it.date - avatarBefore }
                .also { log info "${it.debug}" }
        )

        return FloatingResponse.getAnswer().also {
            it.message = braceTransparentAnswer
            it.image = avatar.profileImage
            it.video = downloadUrl
            it.background = background.url
            it.pose = avatar.talkPose
            it.expectedIntents = expectedIntents.toTypedArray()

            talkLog.lastProc = "TALK_OK"
        }
    }
}
