package ai.maum.ai_human_api.websocket.floating.message.upstream

data class KeepAliveInput(
    var text: String
)
