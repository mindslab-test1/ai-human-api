package ai.maum.ai_human_api.websocket.floating.message.upstream

data class AudioInput(
    var audio: String
)
