package ai.maum.ai_human_api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.ServletContextInitializer
import org.springframework.web.util.WebAppRootListener
import javax.servlet.ServletContext
import javax.servlet.ServletException

@SpringBootApplication
class AiHumanApiApplication : ServletContextInitializer {
    @Throws(ServletException::class)
    override fun onStartup(servletContext: ServletContext) {
        servletContext.addListener(WebAppRootListener::class.java)
        servletContext.setInitParameter("org.apache.tomcat.websocket.textBufferSize", "10240000")
    }
}

fun main(args: Array<String>) {
    runApplication<AiHumanApiApplication>(*args)
}
