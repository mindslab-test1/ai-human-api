package ai.maum.ai_human_api.sds

data class MrcMeta(
    var id: String?,
    var title: String?,
    var sentence: String?,
    var context: String?,
    var frequency: Long?,
    var answerProb: Double?,
    var searchProb: Double?,
    var sentenceProb: Double?
)
