package ai.maum.ai_human_api.sds

import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class Sds(
    private val restTemplate: RestTemplate,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    @Value("\${human.sds.utter.location}")
    lateinit var sdsUtterLocation: String

    @Value("\${human.sds.debug.location}")
    lateinit var sdsDebugLocation: String

    fun run(text: String, host: Long, sessionId: String): SdsRunResponse {
        log info "Host: $host"
        log info "Text: $text"

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        val body = SdsRunRequest(
            host = "$host",
            session = sessionId,
            data = SdsRunRequest.Data(
                utter = text
            ),
            lang = "1"
        )
        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange(
            sdsUtterLocation,
            HttpMethod.POST,
            requestEntity,
            object : ParameterizedTypeReference<SdsRunResponse?>() {}
        )

        log info "Answer: ${response.body?.answer?.answer}"

        return response.body!!
    }

    fun debug(sessionId: String): SdsDebugResponse {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        val body = SdsDebugRequest(
            roomSession = sessionId
        )
        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange(
            sdsDebugLocation,
            HttpMethod.POST,
            requestEntity,
            object : ParameterizedTypeReference<SdsDebugResponse?>() {}
        )

        return response.body!!
    }
}
