package ai.maum.ai_human_api.sds

data class SdsRunRequest(
    var host: String, // 이지만 Long
    var session: String,
    var data: Data,
    var lang: String // 이지만 Long
) {
    data class Data(
        var utter: String
    )
}
