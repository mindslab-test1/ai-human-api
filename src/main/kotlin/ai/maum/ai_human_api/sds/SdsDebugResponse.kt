package ai.maum.ai_human_api.sds

data class SdsDebugResponse(
    var prob: Double?,
    var debugJson: String?,
    var answer: String?,
    var utter: String?,
    var session: String?,
    var host: Long?,
    var id: Long?,
    var lang: Long?,
    var intent: String?,
    var createDate: Long?
) {
    data class DebugJson(
        var input: String?,
        var metaData: String?,
        var engine: String?,
        var replace: String?,
        var type: String?,
        var model: String?,
        var prob: String?,
        var prevIntent: String?,
        var bertIntent: String?,
        var taskRel: String?,
        var ERROR: String?,
        var sdsLog: String?
    )
}
