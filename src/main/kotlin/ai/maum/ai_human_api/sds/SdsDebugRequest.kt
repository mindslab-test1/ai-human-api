package ai.maum.ai_human_api.sds

data class SdsDebugRequest(
    var roomSession: String
)
