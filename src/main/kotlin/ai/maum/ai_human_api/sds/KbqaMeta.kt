package ai.maum.ai_human_api.sds

data class KbqaMeta(
    var map: Map?
) {
    data class Map(
        var subject: String?,
        var property: String?
    )
}
