package ai.maum.ai_human_api.sds

data class SdsRunResponse(
    var answer: Answer?,
    var expectedIntents: Array<ExpectedIntentsItem>?,
    var setMemory: SetMemory?,
    var display: String?,
    var meta: Meta?,
    var jsonDebug: String?
) {
    data class Answer(
        var answer: String?
    )

    data class ExpectedIntentsItem(
        var intent: String?,
        var hierarchy: Long?,
        var entityFlag: Boolean?,
        var displayName: String?,
        var displayType: String?,
        var displayUrl: String?,
        var displayText: String?

    )

    data class SetMemory(
        var utter: Utter?,
        var intent: Intent?,
        var entities: Entities?,
        var entitySet: EntitySet?,
        var lifespan: Long?,
        var host: String?, // String의 탈을 쓴 Long
        var session: String?
    ) {
        data class Utter(
            var utter: String?
        )

        data class Intent(
            var intent: String?,
            var hierarchy: Long?,
            var entityFlag: Boolean?
        )

        class Entities
        data class EntitySet(
            var cust_name: String?,
            var cust_age: String? // String의 탈을 쓴 Long
        )
    }

    data class Meta(
        var intentRel: IntentRelItem?,
        var restApiDto: RestApiDto?
    ) {
        data class IntentRelItem(
            var srcIntent: String?,
            var bertIntent: String?,
            var destIntent: String?
        )
        data class RestApiDto(
            var resultCode: String?,
            var utter: String?,
            var intent: String?,
            var engine: String?,
            var probability: Double?,
            var entities: Array<Entity>?,
            var meta: String?
        ) {
            data class Entity(
                var entityName: String?,
                var entityValue: String?
            )
        }
    }
}
