package ai.maum.ai_human_api.lipsync.check

data class LipSyncProcessInfo(
    var statusCode: Long,
    var waiting: Long
)
