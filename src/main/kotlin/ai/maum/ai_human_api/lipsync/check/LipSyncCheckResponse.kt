package ai.maum.ai_human_api.lipsync.check

data class LipSyncCheckResponse(
    var message: Message? = null,
    var payload: Payload? = null
) {
    data class Message(
        var message: String? = null,
        var status: Long? = null
    )

    data class Payload(
        var statusCode: Long? = null,
        var message: String? = null,
        var waiting: Long? = null
    )
}
