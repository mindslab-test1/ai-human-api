package ai.maum.ai_human_api.lipsync.check

data class LipSyncCheckRequest(
    var apiId: String,
    var apiKey: String,
    var requestKey: String
)
