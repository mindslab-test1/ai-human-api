package ai.maum.ai_human_api.lipsync

import ai.maum.ai_human_api.lipsync.check.LipSyncCheckRequest
import ai.maum.ai_human_api.lipsync.check.LipSyncCheckResponse
import ai.maum.ai_human_api.lipsync.check.LipSyncProcessInfo
import ai.maum.ai_human_api.lipsync.download.LipSyncDownloadRequest
import ai.maum.ai_human_api.lipsync.upload.LipSyncUploadInfo
import ai.maum.ai_human_api.lipsync.upload.LipSyncUploadResponse
import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream

@Service
class LipSync(
    private val restTemplate: RestTemplate,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    @Value("\${human.lipsync.upload.location}")
    lateinit var lipSyncUploadUrl: String

    @Value("\${human.lipsync.statuscheck.location}")
    lateinit var lipSyncStatusCheckUrl: String

    @Value("\${human.lipsync.download.location}")
    lateinit var lipSyncDownloadUrl: String

    fun runUpload(text: String): LipSyncUploadInfo {
        log info "Input Text: $text"

        val headers = HttpHeaders()
        headers.contentType = MediaType.MULTIPART_FORM_DATA

        val sessionData = sessionDataFactory.`object`
        val model = sessionData.avatar?.model

        log info "Model: $model"

        val byteArray = ClassPathResource("background/chromakey.png").inputStream.readAllBytes()

        val resource: ByteArrayResource? =
            // Do not use ?.let here
            // see https://github.com/bumptech/glide/issues/2923
            if (byteArray != null) {
                object : ByteArrayResource(byteArray) {
                    override fun getFilename(): String {
                        return "background.png"
                    }
                }
            } else null

        val map: MultiValueMap<String, Any> = LinkedMultiValueMap()
        map.add("apiId", "mindslab-api-ai-human")
        map.add("apiKey", "e9f409d94c8a4389b55c83cded3e0e3b")
        map.add("text", text)
        resource?.let {
            map.add("image", resource)
        }
        model?.let {
            map.add("model", model)
        }
        map.add("resolution", "FHD")

        val response = restTemplate
            .postForObject(
                lipSyncUploadUrl,
                map,
                LipSyncUploadResponse::class.java
            )

        log info "runUpload Done"

        return LipSyncUploadInfo(response!!.payload!!.requestKey!!)
    }

    fun runCheck(requestKey: String): LipSyncProcessInfo {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        val body = LipSyncCheckRequest(
            apiId = "mindslab-api-ai-human",
            apiKey = "e9f409d94c8a4389b55c83cded3e0e3b",
            requestKey = requestKey
        )
        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange(
            lipSyncStatusCheckUrl,
            HttpMethod.POST,
            requestEntity,
            object : ParameterizedTypeReference<LipSyncCheckResponse?>() {}
        )

        val payload = response.body!!.payload!!

        log debug "Status Code: ${payload.statusCode}"
        log debug "Waiting: ${payload.waiting}"

        return LipSyncProcessInfo(
            statusCode = payload.statusCode!!,
            waiting = payload.waiting!!
        )
    }

    fun runDownload(requestKey: String): Resource {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        val body = LipSyncDownloadRequest(
            apiId = "mindslab-api-ai-human",
            apiKey = "e9f409d94c8a4389b55c83cded3e0e3b",
            requestKey = requestKey
        )
        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange(
            lipSyncDownloadUrl,
            HttpMethod.POST,
            requestEntity,
            object : ParameterizedTypeReference<ByteArray?>() {}
        )

        log info "Video downloaded: requestKey=$requestKey"

        val file = object : ByteArrayResource(response.body!!) {
            override fun getFilename(): String {
                return "$requestKey.mp4"
            }
        }

        return file
    }

    private fun copyInputStreamToFile(inputStream: InputStream, file: File) {
        FileOutputStream(file).use { outputStream ->
            var read: Int
            val bytes = ByteArray(1024)
            while (inputStream.read(bytes).also { read = it } != -1) {
                outputStream.write(bytes, 0, read)
            }
        }
    }
}
