package ai.maum.ai_human_api.lipsync.upload

data class LipSyncUploadResponse(
    var message: Message? = null,
    var payload: Payload? = null
) {
    data class Message(
        var message: String? = null,
        var status: Long? = null
    )

    data class Payload(
        var requestKey: String? = null
    )
}
