package ai.maum.ai_human_api.lipsync.upload

data class LipSyncUploadInfo(
    var requestKey: String
)
