package ai.maum.ai_human_api.lipsync.upload

import java.io.File

data class LipSyncUploadRequest(
    var apiId: String,
    var apiKey: String,
    var text: String,
    var image: File,
    var resolution: String
)
