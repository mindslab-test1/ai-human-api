package ai.maum.ai_human_api.lipsync.download

data class LipSyncDownloadRequest(
    var apiId: String,
    var apiKey: String,
    var requestKey: String
)
