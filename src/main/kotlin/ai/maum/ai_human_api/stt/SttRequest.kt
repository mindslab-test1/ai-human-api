package ai.maum.ai_human_api.stt

import org.springframework.core.io.Resource

data class SttRequest(
    var apiId: String,
    var apiKey: String,
    var lang: String,
    var model: String,
    var samplerate: Long,
    var file: Resource
)
