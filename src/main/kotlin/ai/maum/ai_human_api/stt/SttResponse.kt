package ai.maum.ai_human_api.stt

data class SttResponse(
    var commonMsg: CommonMsg,
    var result: Array<ResultItem>
) {
    data class CommonMsg(
        var message: String,
        var status: Long
    )

    data class ResultItem(
        var start: Double,
        var end: Double,
        var txt: String
    )
}
