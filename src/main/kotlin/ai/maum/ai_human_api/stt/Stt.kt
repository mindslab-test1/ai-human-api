package ai.maum.ai_human_api.stt

import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import java.time.Instant

@Service
class Stt(
    private val restTemplate: RestTemplate,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    @Value("\${human.stt.location}")
    lateinit var sttLocation: String

    fun run(voice: ByteArray): SttText {
        val timestamp = Instant.now().toEpochMilli().toString()
        val hashCode = voice.hashCode() % 1000
        val generatedFileName = timestamp + "_" + hashCode.toString() + ".wav"

        val file = object : ByteArrayResource(voice) {
            override fun getFilename(): String {
                return generatedFileName
            }
        }

        val headers = HttpHeaders()
        headers.contentType = MediaType.MULTIPART_FORM_DATA

        val body: MultiValueMap<String, Any> = LinkedMultiValueMap()
        body.add("apiId", "mindslab-api-ai-human")
        body.add("apiKey", "e9f409d94c8a4389b55c83cded3e0e3b")
        body.add("lang", "kor")
        body.add("samplerate", 8000)
        body.add("model", "w2l_general200_0")
        body.add("file", file)
        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange(sttLocation, HttpMethod.POST, requestEntity, object : ParameterizedTypeReference<SttResponse?>() {})

        var text = ""
        for (fragment in response.body!!.result) {
            text += fragment.txt.replace("\n", "")
            text += " "
        }
        text = text.trimEnd()

        log info "Result: $text"

        return SttText(text)
    }
}
