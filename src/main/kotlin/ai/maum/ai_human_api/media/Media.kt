package ai.maum.ai_human_api.media

import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.core.io.Resource
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.multipart.MultipartFile

@Service
class Media(
    private val restTemplate: RestTemplate,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    @Value("\${human.media.location}")
    lateinit var mediaServerLocation: String

    @Value("\${human.media.download.location}")
    lateinit var mediaServerDownloadLocation: String

    @Value("\${human.media.upload.path}")
    lateinit var mediaServerUploadLocation: String

    fun convertDownloadUrl(uploadUrl: String): String {
        return uploadUrl.replace(mediaServerLocation, mediaServerDownloadLocation)
    }

    data class MediaUrl(
        var pathUrl: String,
        var fullUrl: String
    )

    fun getUrl(filename: String): MediaUrl {
        log info "Filename: $filename"

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        val body = emptyMap<String, Any>().toMutableMap()
        body["filename"] = filename
        body["uploadDirectory"] = mediaServerUploadLocation

        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange("$mediaServerLocation/media/file/path:get", HttpMethod.POST, requestEntity, object : ParameterizedTypeReference<MediaFileUrlResponse?>() {})

        log info response.body.toString()

        val responseBody = response.body!!

        return MediaUrl(pathUrl = responseBody.pathUrl!!, fullUrl = responseBody.fullPathUrl!!)
    }

    fun uploadToMediaServer(file: MultipartFile, url: String): Boolean {
        log info "Url: $url"

        val headers = HttpHeaders()
        headers.contentType = MediaType.MULTIPART_FORM_DATA

        val body: MultiValueMap<String, Any> = LinkedMultiValueMap()
        body.add("file", file.resource)
        body.add("uploadDirectory", url.substring(6))

        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange("$mediaServerLocation/media/file:upload", HttpMethod.POST, requestEntity, object : ParameterizedTypeReference<String?>() {})

        log info response.body.toString()

        return response.statusCode == HttpStatus.CREATED
    }

    fun uploadToMediaServer(file: Resource, url: String): Boolean {
        log info "Url: $url"

        val headers = HttpHeaders()
        headers.contentType = MediaType.MULTIPART_FORM_DATA

        val body: MultiValueMap<String, Any> = LinkedMultiValueMap()
        body.add("file", file)
        body.add("uploadDirectory", url.substring(6))

        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange("$mediaServerLocation/media/file:upload", HttpMethod.POST, requestEntity, object : ParameterizedTypeReference<String?>() {})

        log info response.body.toString()

        return response.statusCode == HttpStatus.CREATED
    }
}
