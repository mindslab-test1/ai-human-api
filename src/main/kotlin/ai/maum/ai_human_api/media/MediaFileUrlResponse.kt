package ai.maum.ai_human_api.media

data class MediaFileUrlResponse(
    var fullPathUrl: String? = null,
    var pathUrl: String? = null
)
