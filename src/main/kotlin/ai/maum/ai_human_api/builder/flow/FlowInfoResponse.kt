package ai.maum.ai_human_api.builder.flow

data class FlowInfoResponse(
    var avatar: String? = null,
    var scenario: Long? = null,
    var background: Long? = null,
    var email: String? = null
)
