package ai.maum.ai_human_api.builder.flow

import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class Flow(
    private val restTemplate: RestTemplate,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    @Value("\${human.flow.info.location}")
    lateinit var flowInfoLocation: String

    fun getFlowInfo(flowId: Long): FlowInfo {
        log info "Requesting Flow Id: $flowId"

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val body = FlowInfoRequest(flowId)

        val requestEntity = HttpEntity(body, headers)

        val response = restTemplate.exchange(flowInfoLocation, HttpMethod.POST, requestEntity, object : ParameterizedTypeReference<FlowInfoResponse>() {})

        log info "Response Body: ${response.body}"

        return FlowInfo.of(response.body!!)
    }
}
