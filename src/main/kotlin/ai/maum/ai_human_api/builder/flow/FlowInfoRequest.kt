package ai.maum.ai_human_api.builder.flow

data class FlowInfoRequest(
    var flowId: Long? = null
)
