package ai.maum.ai_human_api.builder.flow

data class FlowInfo(
    var avatar: String,
    var scenario: Long,
    var background: Long,
    var email: String
) {
    companion object {
        fun of(flowInfoResponse: FlowInfoResponse): FlowInfo {
            return FlowInfo(
                avatar = flowInfoResponse.avatar!!,
                scenario = flowInfoResponse.scenario!!,
                background = flowInfoResponse.background!!,
                email = flowInfoResponse.email ?: ""
            )
        }
    }
}
