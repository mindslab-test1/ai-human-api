package ai.maum.ai_human_api.builder.license

data class LicenseValidationRequest(
    var email: String? = null
)
