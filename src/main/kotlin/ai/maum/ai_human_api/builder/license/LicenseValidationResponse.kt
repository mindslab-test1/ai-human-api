package ai.maum.ai_human_api.builder.license

class LicenseValidationResponse {
    data class Purchased(
        var model: String? = null,
        var licenseKey: String? = null,
        var purchased: String? = null
    )
}
