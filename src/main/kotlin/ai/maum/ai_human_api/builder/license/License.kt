package ai.maum.ai_human_api.builder.license

import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class License(
    private val restTemplate: RestTemplate,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    @Value("\${human.license.validation.location}")
    lateinit var licenseValidationLocation: String

    fun validateLicense(email: String): LicenseValidation {
        log info "Requesting User: $email"

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val body = LicenseValidationRequest(
            email = email
        )

        val requestEntity = HttpEntity(body, headers)

        val response = restTemplate.exchange(
            "$licenseValidationLocation?email=${body.email}",
            HttpMethod.GET,
            requestEntity,
            object : ParameterizedTypeReference<List<LicenseValidationResponse.Purchased>>() {}
        )

        log info "Response Body: ${response.body}"

        val model = sessionDataFactory.`object`.avatar!!.model!!

        return LicenseValidation(response.body!!.any { it.model == model && it.purchased == "Y" })
    }
}
