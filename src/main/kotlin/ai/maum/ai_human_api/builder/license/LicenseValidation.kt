package ai.maum.ai_human_api.builder.license

data class LicenseValidation(
    var valid: Boolean
)
