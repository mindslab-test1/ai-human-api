package ai.maum.ai_human_api.jpa.talklog

import ai.maum.ai_human_api.jpa.AllOpen
import ai.maum.ai_human_api.jpa.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class TalkLog : BaseEntity() {
    @Id
    @SequenceGenerator(
        name = "TALKLOG_SEQ_GEN",
        sequenceName = "TALKLOG_SEQ",
        initialValue = 1,
        allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TALKLOG_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    var sessionId: String? = null

    var flowId: Long? = null

    var avatarModel: String? = null

    var backgroundName: String? = null

    var scenarioHost: Long? = null

    @Column(length = 1024)
    var inputText: String? = null

    @Column(length = 1024)
    var outputText: String? = null

    var videoUrl: String? = null

    var lastProc: String? = null
}
