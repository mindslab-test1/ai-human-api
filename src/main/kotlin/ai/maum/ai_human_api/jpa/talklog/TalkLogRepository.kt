package ai.maum.ai_human_api.jpa.talklog

import org.springframework.data.repository.CrudRepository

interface TalkLogRepository : CrudRepository<TalkLog, Long>
