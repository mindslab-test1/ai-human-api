package ai.maum.ai_human_api.jpa.background

import ai.maum.ai_human_api.jpa.AllOpen
import ai.maum.ai_human_api.jpa.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Background : BaseEntity() {
    @Id
    @SequenceGenerator(
        name = "BACKGROUND_SEQ_GEN",
        sequenceName = "BACKGROUND_SEQ",
        initialValue = 1,
        allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BACKGROUND_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var name: String? = null

    @Column(nullable = false)
    var url: String? = null
}
