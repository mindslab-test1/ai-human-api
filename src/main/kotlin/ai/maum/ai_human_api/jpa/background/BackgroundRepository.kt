package ai.maum.ai_human_api.jpa.background

import org.springframework.data.repository.CrudRepository

interface BackgroundRepository : CrudRepository<Background, Long>
