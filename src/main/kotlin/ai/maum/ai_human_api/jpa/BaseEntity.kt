package ai.maum.ai_human_api.jpa

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.Instant
import javax.persistence.Column
import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass

@AllOpen
@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
class BaseEntity() {
    @CreatedDate
    @Column(nullable = false, updatable = false)
    var created: Instant? = null

    @LastModifiedDate
    @Column(nullable = false, updatable = true)
    var updated: Instant? = null
}
