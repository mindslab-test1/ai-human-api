package ai.maum.ai_human_api.jpa.avatar

import ai.maum.ai_human_api.jpa.AllOpen
import ai.maum.ai_human_api.jpa.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Avatar : BaseEntity() {
    @Id
    @SequenceGenerator(
        name = "AVATAR_SEQ_GEN",
        sequenceName = "AVATAR_SEQ",
        initialValue = 1,
        allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AVATAR_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var model: String? = null

    var profileImage: String? = null

    var muteVideo: String? = null

    var mutePose: String? = null

    var talkPose: String? = null
}
