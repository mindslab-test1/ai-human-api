package ai.maum.ai_human_api.jpa.avatar

import org.springframework.data.repository.CrudRepository

interface AvatarRepository : CrudRepository<Avatar, Long> {
    fun findTopByModel(model: String): Avatar?
}
