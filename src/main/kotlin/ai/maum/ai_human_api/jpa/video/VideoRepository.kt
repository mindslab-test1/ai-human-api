package ai.maum.ai_human_api.jpa.video

import ai.maum.ai_human_api.jpa.avatar.Avatar
import org.springframework.data.repository.CrudRepository

interface VideoRepository : CrudRepository<Video, Long> {
    fun findTopByAvatarAndAnswer(avatar: Avatar, answer: String): Video?
}
