package ai.maum.ai_human_api.jpa.video

import ai.maum.ai_human_api.jpa.AllOpen
import ai.maum.ai_human_api.jpa.BaseEntity
import ai.maum.ai_human_api.jpa.avatar.Avatar
import javax.persistence.*

@AllOpen
@Entity
class Video : BaseEntity() {
    @Id
    @SequenceGenerator(
        name = "VIDEO_SEQ_GEN",
        sequenceName = "VIDEO_SEQ",
        initialValue = 1,
        allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VIDEO_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @ManyToOne(fetch = FetchType.LAZY)
    var avatar: Avatar? = null

    @Column(nullable = false)
    var answer: String? = null

    @Column(nullable = false)
    var url: String? = null
}
