package ai.maum.ai_human_api.jpa.scenario

import org.springframework.data.repository.CrudRepository

interface ScenarioRepository : CrudRepository<Scenario, Long> {
    fun findTopByHost(host: Long): Scenario?
}
