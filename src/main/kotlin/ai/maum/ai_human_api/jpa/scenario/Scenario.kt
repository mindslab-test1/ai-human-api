package ai.maum.ai_human_api.jpa.scenario

import ai.maum.ai_human_api.jpa.AllOpen
import ai.maum.ai_human_api.jpa.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Scenario : BaseEntity() {
    @Id
    @SequenceGenerator(
        name = "SCENARIO_SEQ_GEN",
        sequenceName = "SCENARIO_SEQ",
        initialValue = 1,
        allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SCENARIO_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var host: Long? = null
}
