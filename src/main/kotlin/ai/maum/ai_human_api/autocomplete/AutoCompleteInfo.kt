package ai.maum.ai_human_api.autocomplete

data class AutoCompleteInfo(
    var data: List<AutoCompleteElement>
) {
    data class AutoCompleteElement(
        var text: String,
        var score: Double
    )
}
