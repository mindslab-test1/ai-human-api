package ai.maum.ai_human_api.autocomplete

import com.fasterxml.jackson.annotation.JsonProperty

data class AutoCompleteResponse(
    var took: Long?,
    var timed_out: Boolean?,
    @JsonProperty("_shards")
    var shards: Shards?,
    var hits: Hits?
) {
    data class Shards(
        var total: Long?,
        var successful: Long?,
        var skipped: Long?,
        var failed: Long?
    )

    data class Hits(
        var total: Total?,
        var max_score: Double?,
        var hits: Array<HitsHits>?
    ) {
        data class Total(
            var value: Long?,
            var relation: String?
        )
        data class HitsHits(
            @JsonProperty("_index")
            var index: String?,
            @JsonProperty("_type")
            var type: String?,
            @JsonProperty("_id")
            var id: String?,
            @JsonProperty("_score")
            var score: Double?,
            @JsonProperty("_source")
            var source: Source?
        ) {
            data class Source(
                @JsonProperty("search_string")
                var searchString: String?
            )
        }
    }
}
