package ai.maum.ai_human_api.autocomplete

import com.fasterxml.jackson.annotation.JsonProperty

data class AutoCompleteRequest(
    var query: Query
) {
    data class Query(
        var match: Match
    ) {
        data class Match(
            @JsonProperty("search_string.ngram")
            var search_string: String
        )
    }
}
