package ai.maum.ai_human_api.autocomplete

import ai.maum.ai_human_api.util.logger.WebSocketSessionLogger
import ai.maum.ai_human_api.websocket.SessionData
import ai.maum.ai_human_api.websocket.floating.message.upstream.AutoCompleteInput
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class AutoComplete(
    private val restTemplate: RestTemplate,
    private val sessionDataFactory: ObjectFactory<SessionData>
) {
    private val log = WebSocketSessionLogger(sessionDataFactory, LoggerFactory.getLogger(this.javaClass))

    @Value("\${human.autocomplete.location}")
    lateinit var autoCompleteLocation: String

    operator fun invoke(message: AutoCompleteInput): AutoCompleteInfo {
        val text = message.text

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        val requestBody = AutoCompleteRequest(
            AutoCompleteRequest.Query(
                AutoCompleteRequest.Query.Match(text)
            )
        )
        val requestEntity = HttpEntity(requestBody, headers)

        log debug "Input Text: ${requestBody.query.match.search_string}"
        log debug "Length: ${requestBody.query.match.search_string.length}"
        log debug "Request Body: $requestBody"

        val response = restTemplate.exchange(
            autoCompleteLocation,
            HttpMethod.POST,
            requestEntity,
            object : ParameterizedTypeReference<AutoCompleteResponse?> () {}
        )

        log debug "Result: ${response.statusCode}, ${response.body!!.hits!!}"

        val resultMap = response.body!!.hits!!.hits!!.mapNotNull {
            if (it.source?.searchString == null)
                return@mapNotNull null
            if (it.score == null)
                return@mapNotNull null
            AutoCompleteInfo.AutoCompleteElement(
                it.source?.searchString!!,
                it.score!!
            )
        }

        log debug "ResultMap: $resultMap"

        return AutoCompleteInfo(resultMap)
    }
}
