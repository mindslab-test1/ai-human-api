# Ai Human Api

- [Ai Human Api](#ai-human-api)
  - [Information](#information)
    - [Links](#links)
    - [Tech Stack](#tech-stack)

## Information

### Links

- [Bitbucket](https://pms.maum.ai/bitbucket/projects/SERVICEDEV/repos/ai-human-api/browse)
- [Jira](https://maum.ai/)
- [Confluence](https://pms.maum.ai/confluence/pages/viewpage.action?pageId=35897298)

### Tech Stack

- Kotlin (v1.5.0) JDK 16
- Spring Boot (v2.5.0)
- Oracle
