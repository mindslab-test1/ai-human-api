import com.google.protobuf.gradle.*
import io.gitlab.arturbosch.detekt.Detekt
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jmailen.gradle.kotlinter.tasks.LintTask

// gRPC dependencies
val grpcVersion = "1.38.1"
val grpcKotlinVersion = "1.1.0"
val grpcKotlinExt = "jdk7@jar"
val protobufVersion = "3.17.3"

// gRPC directory configurations
val grpcGeneratedDir = ".grpc_generated"
val grpcOutputSubDir = "grpc"
val grpcKtOutputSubDir = "grpckt"

plugins {
    application
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("org.springframework.boot") version "2.5.2"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.5.0"
    id("org.jmailen.kotlinter") version "3.4.4"
    id("io.gitlab.arturbosch.detekt") version "1.16.0"
    id("org.jetbrains.dokka") version "1.4.32"
    id("com.google.protobuf") version "0.8.14"
    kotlin("jvm") version "1.5.0"
    kotlin("plugin.jpa") version "1.5.0"
    kotlin("plugin.spring") version "1.5.0"
}

group = "ai.maum"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_16

allOpen {
    annotation("ai.maum.ai_human_api.jpa.AllOpen")
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.0")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-websocket")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    implementation("io.github.lognet:grpc-spring-boot-starter:4.2.2")

    implementation("io.grpc:grpc-kotlin-stub:$grpcKotlinVersion")
    implementation("io.grpc:grpc-netty:$grpcVersion")
    // implementation("io.grpc:grpc-protobuf:$grpcVersion")
    // implementation("io.grpc:grpc-services:$grpcVersion")
    implementation("io.grpc:grpc-stub:$grpcVersion")
    runtimeOnly("io.grpc:grpc-netty-shaded:$grpcVersion")

    implementation("org.apache.httpcomponents:httpclient:4.5.13")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    runtimeOnly("com.h2database:h2")
    implementation("org.mariadb.jdbc:mariadb-java-client:2.7.2")

    implementation("org.webjars:webjars-locator-core")
    implementation("org.webjars:sockjs-client:1.0.2")
    implementation("org.webjars:stomp-websocket:2.3.3")
    implementation("org.webjars:bootstrap:3.3.7")
    implementation("org.webjars:jquery:3.1.1-1")
}

kotlinter {
    ignoreFailures = false
    indentSize = 4
    reporters = arrayOf("checkstyle", "plain")
    experimentalRules = false
    disabledRules = arrayOf("no-wildcard-imports")
}

detekt {
    buildUponDefaultConfig = true // preconfigure defaults
    allRules = false // activate all available (even unstable) rules.
    config = files("$projectDir/src/main/resources/detekt/default-detekt-config.yml") // point to your custom config defining rules to run, overwriting default behavior
    baseline = file("$projectDir/src/main/resources/detekt/baseline.xml") // a way of suppressing issues before introducing detekt

    reports {
        html.enabled = true // observe findings in your browser with structure and code snippets
        xml.enabled = true // checkstyle like format mainly for integrations like Jenkins
        txt.enabled = true // similar to the console output, contains issue signature to manually edit baseline files
        sarif.enabled = true // standardized SARIF format (https://sarifweb.azurewebsites.net/) to support integrations with Github Code Scanning
    }
}

kotlin {
    sourceSets["main"].apply {
        kotlin.srcDir("$projectDir/$grpcGeneratedDir/main/$grpcKtOutputSubDir")
    }
}

sourceSets {
    main {
        proto {
            // In addition to the default 'src/main/proto'
            srcDir("src/main/protobuf")
            srcDir("src/main/protocolbuffers")
            // In addition to the default '**/*.proto' (use with caution).
            // Using an extension other than 'proto' is NOT recommended,
            // because when proto files are published along with class files, we can
            // only tell the type of a file from its extension.
            include("**/*.protodevel")
        }
        java {
            srcDir("$grpcGeneratedDir/main/$grpcOutputSubDir")
            srcDir("$grpcGeneratedDir/main/java")
        }
    }
    test {
        proto {
            // In addition to the default 'src/test/proto'
            srcDir("src/test/protocolbuffers")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:$protobufVersion"
    }

    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:$grpcKotlinVersion:$grpcKotlinExt"
        }
    }
    /*
     * (For plugins; ex:grpc, grpckt)
     * Generated stub files will be located at:
     * $generatedFilesBaseDir/$sourceSet/$outputSubDir/{java_package declared in .proto}
     *
     * (For builtins; ex:java)
     * Generated core files will be located at:
     * $generatedFilesBaseDir/$sourceSet/$builtinPluginName
     */
    generateProtoTasks {
        ofSourceSet("main").forEach { task ->
            task.builtins {
                remove("java")
            }
            task.plugins {
                id("grpc") {
                    outputSubDir = grpcOutputSubDir
                }
                id("grpckt") {
                    outputSubDir = grpcKtOutputSubDir
                }
                id("java") {
                    outputSubDir = grpcOutputSubDir
                }
            }
        }
    }
    generatedFilesBaseDir = "$projectDir/$grpcGeneratedDir"
}

tasks.clean {
    delete("$projectDir/$grpcGeneratedDir")
}

configurations {
    all {
        exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
    }
}

tasks {
    "lintKotlinMain"(LintTask::class) {
        exclude("**/BrainLipsyncGrpcKt.kt")
        exclude("**/W2LGrpcKt.kt")
    }
}

tasks.withType<Detekt> {
    onlyIf { project.hasProperty("runDetekt") }
    // Target version of the generated JVM bytecode. It is used for type resolution.
    jvmTarget = "16"
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "16"
    }
}

tasks.withType<Test> {
    useJUnitPlatform {
        includeEngines("junit-vintage")
    }
}
